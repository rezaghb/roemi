           <script>
              function uploadFile(uid) {
                var files;
                if(uid == 1){
                    files = document.getElementById("file").files;
                }else{
                   files = document.getElementById("file2").files;
                }
                  
                  if(files.length > 0 ){
                      var formData = new FormData();
                      formData.append("file", files[0]);
                      var xhttp = new XMLHttpRequest();
                      // Set POST method and ajax file path
                      xhttp.open("POST", "<?=base_url('index.php/upload/own')?>", true);
                      // call on request changes state
                      xhttp.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                              var response = this.responseText;
                              if(response != 0){
                                if(uid == 1){
                                  document.getElementById("images_id").value = response;
                                }else{
                                  document.getElementById("images_en").value = response;
                                }
                              }else{
                                alert("File not uploaded.");
                              }
                          }
                      };
                      // Send request with data
                      xhttp.send(formData);
                  }else{
                      alert("Please select a file");
                  }
              }
            </script>
