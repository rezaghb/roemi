<?php
$lang = "id";


?>

<!DOCTYPE html>
<html lang="english">
    <head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>ROEMI - Official Website</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url()?>assets/images/fav.png">
        <!-- Bootstrap v5.0.2 css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/animate.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/owl.carousel.css">
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/slick.css">
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/off-canvas.css">
        <!-- linea-font css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/fonts/linea-fonts.css">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/fonts/flaticon.css">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/magnific-popup.css">
        <!-- Main Menu css -->
        <link rel="stylesheet" href="<?= base_url()?>assets/css/rsmenu-main.css">
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/rs-spacing.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>style.css"> <!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/responsive.css">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="home-style3">
        
        <!--Preloader area start here-->
        <div id="loader" class="loader">
            <div class="loader-container">
                <div class='loader-icon'>
                    <img src="<?= base_url()?>assets/images/pre-logo.png" alt="">
                </div>
            </div>
        </div>
        <!--Preloader area End here-->

        <!--Full width header Start-->
        <div class="full-width-header header-style2 modify1">
            <!--Header Start-->
            <header id="rs-header" class="rs-header">
                <!-- Topbar Area Start -->
                <div class="topbar-area d-none">
                    <div class="container">
                        <div class="row y-middle">
                            <div class="col-md-6">
                                <ul class="topbar-contact">
                                    <li>
                                        <i class="flaticon-email"></i>
                                        <a href="mailto:support@rstheme.com">support@rstheme.com</a>
                                    </li>
                                    <li>
                                        <i class="flaticon-call"></i>
                                        <a href="tel:+0885898745">(+088) 589-8745</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 text-end">
                                <ul class="topbar-right">
                                    <li class="login-register">
                                        <i class="fa fa-sign-in"></i>
                                        <a href="login.html">Login</a>/<a href="register.html">Register</a>
                                    </li>
                                    <li class="btn-part">
                                        <a class="apply-btn" href="#">Apply Now</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Topbar Area End -->

                <!-- Menu Start -->
                <div class="menu-area menu-sticky">
                    <div class="container-fluid">
                        <div class="row y-middle">
                            <div class="col-lg-4">
                                <div class="logo-cat-wrap">
                                    <div class="logo-part pr-90">
                                        <a href="<?= base_url()?>">
                                            <img src="<?= base_url()?>assets/images/roemi.png" alt="">
                                        </a>
                                    </div>
                                    <!-- <div class="categories-btn">
                                       <button type="button" class="cat-btn"><i class="fa fa-th"></i>Categories</button>
                                        <div class="cat-menu-inner">
                                            <ul id="cat-menu">
                                                <li><a href="">Category 1</a></li>
                                                <li><a href="">Category 2</a></li>
                                                <li><a href="">Category 3</a></li>
                                                <li><a href="">Category 4</a></li>
                                            </ul>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="col-lg-8 text-center">
                                <div class="rs-menu-area">
                                    <div class="main-menu pr-90 md-pr-15">
                                        <div class="mobile-menu">
                                            <a class="rs-menu-toggle">
                                                <i class="fa fa-bars"></i>
                                            </a>
                                        </div>
                                        <nav class="rs-menu">
                                           <ul class="nav-menu">
                                              <li class="rs-mega-menu mega-rs current-menu-item"> <a href="<?= base_url()?>">Home</a></li>
                                              <li class="rs-mega-menu mega-rs "> <a href="<?= base_url("index.php/pages/about/".$lang)?>">About Us</a></li>
                                              <li class="rs-mega-menu mega-rs "> <a href="<?= base_url("index.php/pages/works/".$lang)?>">Our Works</a></li>
                                              <li class="rs-mega-menu mega-rs "> <a href="<?= base_url("index.php/pages/resources/".$lang)?>">Resources</a></li>
                                              <li class="rs-mega-menu mega-rs "> <a href="<?= base_url("index.php/pages/partners/".$lang)?>">Partners</a></li>
                                              
                                              <li class="menu-item-has-children">
                                                   <a href="<?= base_url("index.php/pages/involved/".$lang)?>">Involved</a>
                                                   <ul class="sub-menu">
                                                       <li><a href="about.html">Become a member</a> </li>
                                                       <li><a href="about2.html">Subscribe</a> </li>
                                                       <li><a href="about2.html">Become donor</a> </li>
                                                   </ul>
                                               </li>

                                              <li class="rs-mega-menu mega-rs"> <a href="<?= base_url("index.php/pages/contact")?>">Get in Touch</a></li>


                                           </ul> <!-- //.nav-menu -->
                                        </nav>                                         
                                    </div> <!-- //.main-menu -->
                                    <div class="expand-btn-inner">
                                        <ul>
                                            <li>
                                                <a class="hidden-xs rs-search" data-bs-toggle="modal" data-bs-target="#searchModal" href="#">
                                                    <i class="flaticon-search"></i>
                                                </a>
                                            </li>
                                            <li class="icon-bar cart-inner no-border mini-cart-active">
                                                <a class="cart-icon">
                                                    <!-- <span class="cart-count">2</span> -->
                                                    <!-- <i class="fa fa-language  fa-3x"></i> -->
                                                    <?php
                                                    if($lang == "id"){?>
                                                        <img src="<?=base_url()?>assets/images/indonesia.png" width="16">
                                                    <?php }else{?>
                                                        <img src="<?=base_url()?>assets/images/uk.png" width="16">
                                                    <?php }?>
                                                </a>
                                                <div class="woocommerce-mini-cart text-start">
                                                    <div class="cart-bottom-part">
                                                        <div class="cart-btn text-center">
                                                            <a class="crt-btn btn1" href="<?=base_url("index.php/language/change/id")?>">Bahasa</a>
                                                            <a class="crt-btn btn2" href="<?=base_url("index.php/language/change/en")?>">English</a>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </li>
                                        </ul>
                                        <a id="nav-expander" class="nav-expander style4">
                                          <span class="dot1"></span>
                                          <span class="dot2"></span>
                                          <span class="dot3"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu End -->
                
                <!-- Canvas Menu start -->
                <nav class="right_menu_togle hidden-md">
                    <div class="close-btn">
                        <div id="nav-close">
                            <div class="line">
                                <span class="line1"></span><span class="line2"></span>
                            </div>
                        </div>
                    </div>
                    <div class="canvas-logo">
                        <a href="index.html"><img src="<?= base_url()?>assets/images/logo-dark.png" alt="logo"></a>
                    </div>
                    <div class="offcanvas-text">
                        <p>We denounce with righteous indige nationality and dislike men who are so beguiled and demo  by the charms of pleasure of the moment data com so blinded by desire.</p>
                    </div>
                    <div class="offcanvas-gallery">
                        <div class="gallery-img">
                            <a class="image-popup" href="<?= base_url()?>assets/images/gallery/1.jpg"><img src="<?= base_url()?>assets/images/gallery/1.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="<?= base_url()?>assets/images/gallery/2.jpg"><img src="<?= base_url()?>assets/images/gallery/2.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="<?= base_url()?>assets/images/gallery/3.jpg"><img src="<?= base_url()?>assets/images/gallery/3.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="<?= base_url()?>assets/images/gallery/4.jpg"><img src="<?= base_url()?>assets/images/gallery/4.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="<?= base_url()?>assets/images/gallery/5.jpg"><img src="<?= base_url()?>assets/images/gallery/5.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="<?= base_url()?>assets/images/gallery/6.jpg"><img src="<?= base_url()?>assets/images/gallery/6.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="map-img">
                        <img src="<?= base_url()?>assets/images/roemi-map.jpg" alt="">
                    </div>
                    <div class="canvas-contact">
                        <ul class="social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Canvas Menu end -->
            </header>
            <!--Header End-->
        </div>
        <!--Full width header End-->










        <!-- Main content Start -->
        <div class="main-content">

            <!-- Erro 404 Page Start Here -->
            <div id="rs-page-error" class="rs-page-error">
                <div class="error-text">
                    <h1 class="error-code">404</h1>
                    <h3 class="error-message">Page Not Found</h3>
                    <form>
                        <input type="search" placeholder="Search..." name="s" class="search-input" value="">
                        <button type="submit" value="Search"><i class="fa fa-search"></i></button>
                    </form>
                    <a class="readon orange-btn" href="<?= base_url()?>" title="HOME">Back to Homepage</a>
                </div>
            </div>
            <!-- Erro 404 Page End Here -->
        </div> 
        <!-- Main content End --> 








		       <!-- Footer Start -->
			   <footer id="rs-footer" class="rs-footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-50">
                            <h4 class="widget-title">Explore</h4>
                            <ul class="site-map">
                                <li><a href="<?= base_url("index.php/users/login")?>">Login</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Courses</a></li>
                                <li><a href="#">Become A Teacher</a></li>
                                <li><a href="#">Blog</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-50">
                            <h4 class="widget-title">Categories</h4>
                            <ul class="site-map">
                                <li><a href="#">All Courses</a></li>
                                <li><a href="#">Web Development</a></li>
                                <li><a href="#">Genarel Education</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Web Design</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-50">
                            <h4 class="widget-title">Resources</h4>
                            <ul class="site-map">
                                <li><a href="#">Become A Teacher</a></li>
                                <li><a href="#">Instructor/Student Profile</a></li>
                                <li><a href="#">Courses</a></li>
                                <li><a href="#">Checkout</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 footer-widget">
                            <h4 class="widget-title">Address</h4>
                            <ul class="address-widget">
                                <li>
                                    <i class="flaticon-location"></i>
                                    <div class="desc">503 Old Buffalo Street Northwest #205 New York-3087</div>
                                </li>
                                <li>
                                    <i class="flaticon-call"></i>
                                    <div class="desc">
                                        <a href="tel:(123)-456-7890">(123)-456-7890</a> , 
                                        <a href="tel:(123)-456-7890">(123)-456-7890</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-email"></i>
                                    <div class="desc">
                                        <a href="mailto:hello@roemi.id">hello@roemi.id</a> , 
                                        <a href="#">www.roemi.id</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">                    
                    <div class="row y-middle">
                        <div class="col-lg-4 md-mb-20">
                            <div class="footer-logo md-text-center">
                                <a href="index.html"><img src="<?= base_url()?>assets/images/logo.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-4 md-mb-20">
                            <div class="copyright text-center md-text-start">
                                <p>&copy; <?=date('Y')?> All Rights Reserved. </p>
                            </div>
                        </div>
                        <div class="col-lg-4 text-end md-text-start">
                            <ul class="footer-social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div id="scrollUp">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- End scrollUp  -->

        <!-- Search Modal Start -->
        <div class="modal fade search-modal" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
            <button type="button" class="close" data-bs-dismiss="modal">
              <span class="flaticon-cross"></span>
            </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here..." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- modernizr js -->
        <script src="<?= base_url()?>assets/js/modernizr-2.8.3.min.js"></script>
        <!-- jquery latest version -->
        <script src="<?= base_url()?>assets/js/jquery.min.js"></script>
        <!-- Bootstrap v5.0.2 js -->
        <script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
        <!-- Menu js -->
        <script src="<?= base_url()?>assets/js/rsmenu-main.js"></script> 
        <!-- op nav js -->
        <script src="<?= base_url()?>assets/js/jquery.nav.js"></script>
        <!-- owl.carousel js -->
        <script src="<?= base_url()?>assets/js/owl.carousel.min.js"></script>
        <!-- Slick js -->
        <script src="<?= base_url()?>assets/js/slick.min.js"></script>
        <!-- isotope.pkgd.min js -->
        <script src="<?= base_url()?>assets/js/isotope.pkgd.min.js"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="<?= base_url()?>assets/js/imagesloaded.pkgd.min.js"></script>
        <!-- wow js -->
        <script src="<?= base_url()?>assets/js/wow.min.js"></script>
        <!-- Skill bar js -->
        <script src="<?= base_url()?>assets/js/skill.bars.jquery.js"></script>
        <script src="<?= base_url()?>assets/js/jquery.counterup.min.js"></script>        
         <!-- counter top js -->
        <script src="<?= base_url()?>assets/js/waypoints.min.js"></script>
        <!-- video js -->
        <script src="<?= base_url()?>assets/js/jquery.mb.YTPlayer.min.js"></script>
        <!-- magnific popup js -->
        <script src="<?= base_url()?>assets/js/jquery.magnific-popup.min.js"></script> 
        <!-- parallax-effect js -->
        <script src="<?= base_url()?>assets/js/parallax-effect.min.js"></script>     
        <!-- plugins js -->
        <script src="<?= base_url()?>assets/js/plugins.js"></script>
        <!-- contact form js -->
        <script src="<?= base_url()?>assets/js/contact.form.js"></script>
        <!-- main js -->
        <script src="<?= base_url()?>assets/js/main.js"></script>
    </body>
</html>