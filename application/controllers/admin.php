<?php

class Admin extends CI_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model('m_settings');
        $this->load->model('m_db');
        $this->load->model('m_user');
        $this->load->model('m_contact');
        if(!$this->session->userdata("user_id"))//If already logged in
        {
            redirect(base_url());
        }
    }
    function index()
    {
        $conf = $this->m_db->getByType("confirmation");
        $contact = $this->m_contact->list();
        $user = $this->m_user->list();
        $visitor = $this->m_settings->getByName("visitor");

        $data['confirmation'] = count($conf);
        $data['contact'] = count($contact);
        $data['user'] = count($user);
        $data['visitor'] = $visitor['value'];
        $this->load->view('admin/header');
        $this->load->view('admin/dashboard', $data);
        $this->load->view('admin/footer');
        // redirect(base_url('index.php/admin/pages_event/d962c3ca56c3ac25725bb76882845e3d'));
    }
    function dashboard()
    {
        $this->index();
    }

    // setting


    function setting()
    {
        $data['settings'] = $this->m_settings->list_settings(10,0);
        $this->load->view('admin/header');
        $this->load->view('admin/setting_list',  $data);
        $this->load->view('admin/footer');
    }

    function setting_edit($id)
    {
        $data['setting'] = $this->m_settings->get_settings($id);
        $this->load->view('admin/header');
        $this->load->view('admin/setting_edit',  $data);
        $this->load->view('admin/footer');
    }

    function setting_add()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/setting_add');
        $this->load->view('admin/footer');
    }


      // end setting

      //start page
    function pages()
    {

        $data['pages'] = $this->m_db->get_pages(10,0);
        $this->load->view('admin/header');
        $this->load->view('admin/pages_list',  $data);
        $this->load->view('admin/footer');
    }

    function pages_edit($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_edit', $data);
        $this->load->view('admin/footer');
    }
    function pages_home($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_home', $data);
        $this->load->view('admin/footer');
    }
    function pages_about($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['pages'] = $this->m_db->getByType("personil");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_about', $data);
        $this->load->view('admin/footer');
    }
    function pages_personil()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/pages_personil');
        $this->load->view('admin/footer');
    }
    function pages_personil_edit($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['idx'] =$id;
        $this->load->view('admin/header');
        $this->load->view('admin/pages_personil_edit', $data);
        $this->load->view('admin/footer');
    }

    function pages_member($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_member', $data);
        $this->load->view('admin/footer');
    }
    function pages_subscribe($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_subscribe', $data);
        $this->load->view('admin/footer');
    }

    function pages_pribadi($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_pribadi', $data);
        $this->load->view('admin/footer');
    }
    function pages_lembaga($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_lembaga', $data);
        $this->load->view('admin/footer');
    }
    function pages_faq($id)
    {
        $data['faqs'] = $this->m_db->getByType("faq");
        $this->load->view('admin/header');
        $this->load->view('admin/faq_list', $data);
        $this->load->view('admin/footer');
    }
    function pages_faq_add()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/faq_add');
        $this->load->view('admin/footer');
    }
    function pages_faq_edit($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['idx'] =$id;
        $this->load->view('admin/header');
        $this->load->view('admin/faq_edit', $data);
        $this->load->view('admin/footer');
    }
    function pages_media()
    {

        $this->load->view('admin/header');
        $this->load->view('admin/media_list');
        $this->load->view('admin/footer');
    }
    function pages_event($id)
    {
        $data['pages'] = $this->m_db->getByType("event");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_event', $data);
        $this->load->view('admin/footer');
    }
    function pages_event_add()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/pages_event_add');
        $this->load->view('admin/footer');
    }
    function pages_event_edit($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['idx'] =$id;
        $this->load->view('admin/header');
        $this->load->view('admin/pages_event_edit', $data);
        $this->load->view('admin/footer');
    }

    function pages_event_countdown($startdate)
    {
        $date = strtotime($startdate);
        $remaining = $date - time();

        $days_remaining = floor($remaining / 86400);
        $hours_remaining = floor(($remaining % 86400) / 3600);
        echo "There are $days_remaining days and $hours_remaining hours left";
    }
    function pages_works($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_works', $data);
        $this->load->view('admin/footer');
    }
    function pages_works_koper($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_works_koper', $data);
        $this->load->view('admin/footer');
    }
    function pages_works_reach($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_works_reach', $data);
        $this->load->view('admin/footer');
    }
    function pages_works_reach_news()
    {
        $data['pages'] = $this->m_db->getByType("reach-news");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_reach_news', $data);
        $this->load->view('admin/footer');
    }
    function pages_works_reach_news_add()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/pages_reach_news_add');
        $this->load->view('admin/footer');
    }
    function pages_reach_news_edit($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['idx'] = $id;
        $this->load->view('admin/header');
        $this->load->view('admin/pages_reach_news_edit', $data);
        $this->load->view('admin/footer');
    }
    function pages_resources($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['pages'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_resources', $data);
        $this->load->view('admin/footer');
    }
    function pages_resources_sosmed()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/pages_resources_sosmed');
        $this->load->view('admin/footer');
    }
    function pages_resources_edit($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['idx'] = $id;
        $this->load->view('admin/header');
        $this->load->view('admin/pages_resources_edit',$data);
        $this->load->view('admin/footer');
    }
    function pages_partners($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['pages'] = $this->m_db->getByType("partners-logo");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_partners', $data);
        $this->load->view('admin/footer');
    }
    function pages_partners_sosmed()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/pages_partners_sosmed');
        $this->load->view('admin/footer');
    }

    function pages_partners_edit($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $data['idx'] = $id;
        $this->load->view('admin/header');
        $this->load->view('admin/pages_partners_edit',$data);
        $this->load->view('admin/footer');
    }
    function pages_involved($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_involved', $data);
        $this->load->view('admin/footer');
    }
    function pages_getin_sosmed($id)
    {
        $data['pageid'] = $this->m_db->getByContainer($id, "id");
        $data['pageen'] = $this->m_db->getByContainer($id, "en");
        $this->load->view('admin/header');
        $this->load->view('admin/pages_getin_sosmed', $data);
        $this->load->view('admin/footer');
    }

    function pages_add()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/pages_add');
        $this->load->view('admin/footer');
    }

      //end page

      //start user
      function user()
      {
        $data['users'] = $this->m_user->list();
          $this->load->view('admin/header');
          $this->load->view('admin/user_list', $data);
          $this->load->view('admin/footer');
      }
      function pages_user_add()
      {
          $this->load->view('admin/header');
          $this->load->view('admin/pages_user_add');
          $this->load->view('admin/footer');
      }

     //end user



    function contact()
    {
        $data['contacts'] = $this->m_contact->list();
        $this->load->view('admin/header');
        $this->load->view('admin/contact_list', $data);
        $this->load->view('admin/footer');
    }

    function confirmation()
    {
        $data['pages'] = $this->m_db->getByType("confirmation");
        $this->load->view('admin/header');
        $this->load->view('admin/confirmation_list', $data);
        $this->load->view('admin/footer');
    }
    function about()
    {

        $this->load->view('header');
        $this->load->view('about');
        $this->load->view('footer');
    }
    function works()
    {

        $this->load->view('header');
        $this->load->view('works');
        $this->load->view('footer');
    }
    function resources()
    {

        $this->load->view('header');
        $this->load->view('resources');
        $this->load->view('footer');
    }
    function partners()
    {

        $this->load->view('header');
        $this->load->view('partners');
        $this->load->view('footer');
    }
    function involved()
    {

        $this->load->view('header');
        $this->load->view('involved');
        $this->load->view('footer');
    }

}
