<?php

class ActionPages extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('m_settings');
        $this->load->model('m_db');
        // if(!$this->session->userdata("user_id"))//If already logged in
        // {
        //     redirect(base_url());
        // }
    }

    function add()
    {
        $data['error'] = NULL;
        if($this->input->post())
        {
            $config = array(
                array(
                    'field' => 'title_id',
                    'label' => 'Title Bahasa',
                    'rules' => 'trim|required|min_length[1]|max_length[200]'
                )

            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $type =  ($this->input->post('type'))?$this->input->post('type'):"";
            $redirectSuccess =  $this->input->post('redirect-success');
            $redirectError =  $this->input->post('redirect-error');
            if($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
                $this->session->set_flashdata('msg-alert', "danger");
                $this->session->set_flashdata('msg', "Silahkan input semua form di Tab Bahasa dan English");
                
                redirect(base_url($redirectError));
            }
            else 
            {
                $container = md5(microtime(true));
                $dataId = array(
                    'post_title' => $this->input->post('title_id'),
                    'type' => $type,
                    'container' =>$container,
                    'language' => 'id',
                    'active' => 1,
                    'content1' => $this->input->post('content_id1'),
                    'content2' => $this->input->post('content_id2'),
                    'content3' => $this->input->post('content_id3'),
                    'content4' => $this->input->post('content_id4'),
                    'content5' => $this->input->post('content_id5'),

                );
                $dataEn = array(
                    'post_title' => $this->input->post('title_en'),
                    'type' => $type,
                    'container' =>$container,
                    'language' => 'en',
                    'active' => 1,
                    'content1' => $this->input->post('content_en1'),
                    'content2' => $this->input->post('content_en2'),
                    'content3' => $this->input->post('content_en3'),
                    'content4' => $this->input->post('content_en4'),
                    'content5' => $this->input->post('content_en5'),
                );
                $user_id = $this->m_db->insert_pages($dataId);
                $user_en = $this->m_db->insert_pages($dataEn);
                $this->session->set_flashdata('msg-alert', "success");
                $this->session->set_flashdata('msg', "Success!");
                redirect(base_url($redirectSuccess));
            }
            
        }

        
    }







    function edit()
    {
        $data['error'] = NULL;
        if($this->input->post())
        {


            $config = array(
                array(
                    'field' => 'id-id',
                    'label' => 'id bahasa',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'id-en',
                    'label' => 'id english',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'title_id',
                    'label' => 'Title Bahasa',
                    'rules' => 'trim|required|min_length[1]|max_length[200]'
                ),

            

            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $idid =  $this->input->post('id-id');
            $iden =  $this->input->post('id-en');
            $type =  ($this->input->post('type'))?$this->input->post('type'):"";
            $redirectSuccess =  $this->input->post('redirect-success');
            $redirectError =  $this->input->post('redirect-error');
            if($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
                // $this->session->set_flashdata('msg', validation_errors());
                $this->session->set_flashdata('msg-alert', "danger");
                $this->session->set_flashdata('msg', "Silahkan input semua form di Tab Bahasa dan English");
                
                redirect(base_url($redirectError));
            }
            else 
            {
                $dataId = array(
                    'post_title' => $this->input->post('title_id'),
                    'type' => $type,
                    'content1' => $this->input->post('content_id1'),
                    'content2' => $this->input->post('content_id2'),
                    'content3' => $this->input->post('content_id3'),
                    'content4' => $this->input->post('content_id4'),
                    'content5' => $this->input->post('content_id5'),

                );
                $dataEn = array(
                    'post_title' => $this->input->post('title_en'),
                    'type' => $type,
                    'content1' => $this->input->post('content_en1'),
                    'content2' => $this->input->post('content_en2'),
                    'content3' => $this->input->post('content_en3'),
                    'content4' => $this->input->post('content_en4'),
                    'content5' => $this->input->post('content_en5'),
                );
                $user_id = $this->m_db->update_page($idid, $dataId);
                $user_en = $this->m_db->update_page($iden, $dataEn);
                $this->session->set_flashdata('msg-alert', "success");
                $this->session->set_flashdata('msg', "Success!");
                
                redirect(base_url($redirectSuccess));
            }
            
        }

      
    }



    function approval()
    {
        $data['error'] = NULL;
        if($this->input->post())
        {


            $config = array(
                array(
                    'field' => 'id',
                    'label' => 'id',
                    'rules' => 'trim|required'
                ),
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $idid =  $this->input->post('id');
            if($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
                $this->session->set_flashdata('msg', validation_errors());
                redirect(base_url().'index.php/admin/pages_edit/'.$id);
            }
            else 
            {
                $dataId = array(
                    'content6' => "Approved",
                );
  
                $user_id = $this->m_db->update_page($idid, $dataId);
                redirect(base_url().'index.php/admin/confirmation');
            }
            
        }

      
    }


}