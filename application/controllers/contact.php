<?php

class Contact extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('m_settings');
        $this->load->model('m_contact');
    }

    function add()
    {
        $data['error'] = NULL;
        if($this->input->post())
        {
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'trim|required|min_length[1]|max_length[200]'
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email',
                ),
                array(
                    'field' => 'subject',
                    'label' => 'Subject',
                    'rules' => 'trim|required|min_length[1]|max_length[200]',
                ),
                array(
                    'field' => 'message',
                    'label' => 'Message',
                    'rules' => 'trim|required|min_length[1]|max_length[4000]',
                )
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            if($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
                // $this->session->set_flashdata('msg', validation_errors());
                $this->session->set_flashdata('msg-alert', "danger");
                $this->session->set_flashdata('msg', validation_errors());
                
                redirect(base_url().'index.php/pages/contact');
            }
            else 
            {
                $data = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('message')
                );
                $user_id = $this->m_contact->create($data);
                $this->session->set_flashdata('msg-alert', "success");
                $this->session->set_flashdata('msg', "Success!");
                redirect(base_url().'index.php/pages/contact');
            }
            
        }

        
    }







   
      
    }



