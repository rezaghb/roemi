<?php

class Pages extends CI_Controller
{

    public function __construct() {
        parent::__construct();

        if(!$this->session->userdata('language')){
            $this->session->set_userdata('language', "id");
        }
        $this->load->model('m_db');
        $this->load->model('m_settings');
        $this->lang  = $this->session->userdata('language');

        $visitor = $this->m_settings->getByName("visitor");
        $visitorPlus = array("value" => $visitor['value']+1);
        $this->m_settings->updateByName("visitor", $visitorPlus);

    }
    function index($start = 0)//index page
    {
        $data['page'] = $this->m_db->getByContainer("b991d57f9e35813ca5b90dab68b5f276",$this->lang);
        $data['partners'] = $this->m_db->getByType("partners-logo");
        $this->load->view('header');
        $this->load->view('v_home', $data);
        $this->load->view('footer');
    }
    function contact()
    {
        $data['sosmed'] = $this->m_db->getByContainer("16975d4fc3dd4c858cf9a3c3dfc2ba92","id");
        $this->load->view('header');
        $this->load->view('v_contact', $data);
        $this->load->view('footer');
    }
    function faq()
    {
        $data['faqs'] = $this->m_db->getByType("faq", $this->lang);
        $this->load->view('header');
        $this->load->view('faq', $data);
        $this->load->view('footer');
    }
    function about()
    {
        $data['page'] = $this->m_db->getByContainer("87dc9560c7a13615a38eed3e426a8418",$this->lang);
        $data['personils'] = $this->m_db->getByType("personil");
        $data['faqs'] = $this->m_db->getByType("faq", $this->lang);
        $this->load->view('header');
        $this->load->view('about', $data);
        $this->load->view('footer');
    }
    function inisiatif()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('v_inisiatif', $data);
        $this->load->view('footer');
    }
    function reach()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('reach', $data);
        $this->load->view('footer');
    }
    function koper()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('koper', $data);
        $this->load->view('footer');
    }
    function kolaborator()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('v_kolaborator', $data);
        $this->load->view('footer');
    }
    function join()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('v_yukikut', $data);
        $this->load->view('footer');
    }
    function joinus()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('v_join', $data);
        $this->load->view('footer');
    }
    function foto()
    {
        $this->load->view('header');
        $this->load->view('v_gallery');
        $this->load->view('footer');
    }
    function video()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('v_video', $data);
        $this->load->view('footer');
    }
    function audio()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('v_audio', $data);
        $this->load->view('footer');
    }
    function material()
    {
        $this->load->view('header');
        $this->load->view('v_material');
        $this->load->view('footer');
    }
    function news()
    {
        $this->load->view('header');
        $this->load->view('v_news');
        $this->load->view('footer');
    }
    function event()
    {
        $this->load->view('header');
        $this->load->view('v_event');
        $this->load->view('footer');
    }
    function fans()
    {
        $this->load->view('header');
        $this->load->view('v_fans');
        $this->load->view('footer');
    }
    function investor()
    {
        $this->load->view('header');
        $this->load->view('v_invest');
        $this->load->view('footer');
    }
    function works()
    {
        $data['page'] = $this->m_db->getByContainer("87a00519bc3bcbf74989854fe3ff09da",$this->lang);
        $data['reach'] = $this->m_db->getByContainer("514d8f76d8370fafa68dffc984ad85a9",$this->lang);
        $data['news'] = $this->m_db->getByType("reach-news", $this->lang);
        $this->load->view('header');
        $this->load->view('works', $data);
        $this->load->view('footer');
    }
    function resources()
    {
        $data['resources'] = $this->m_db->getByType("resources-sosmed");
        $this->load->view('header');
        $this->load->view('resources', $data);
        $this->load->view('footer');
    }
    function partners()
    {
        $data['partners'] = $this->m_db->getByType("partners-logo");
        $this->load->view('header');
        $this->load->view('partners', $data);
        $this->load->view('footer');
    }
    function involved()
    {

        $this->load->view('header');
        $this->load->view('involved');
        $this->load->view('footer');
    }
    function donasi_pribadi()
    {
        $data['page'] = $this->m_db->getByContainer("0ce90809ee7460fcce92fcbf3ac263d2","id");
        $data['qr'] = $this->m_db->getByContainer("0ce90809ee7460fcce92fcbf3ac263d2","en");
       
        $this->load->view('header');
        $this->load->view('donor_pribadi', $data);
        $this->load->view('footer');
    }
    function donasi_lembaga()
    {
        $data['page'] = $this->m_db->getByContainer("99576a8a1652c9bcc9930e2d7d385ead",$this->lang);
       
        $this->load->view('header');
        $this->load->view('donor_lembaga', $data);
        $this->load->view('footer');
    }
    function konfirmasi()
    {

        $this->load->view('header');
        $this->load->view('konfirmasi');
        $this->load->view('footer');
    }

}
