<?php

class Language extends CI_Controller
{
    function change($lang)
    {
        if($lang == "id"){
            $this->session->set_userdata('language', "id");
        }elseif($lang == "en"){
            $this->session->set_userdata('language', "en");
        }else{
            $this->session->set_userdata('language', "id");
        }
        redirect(base_url());
    }
}

