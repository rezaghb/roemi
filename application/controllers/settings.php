<?php

class Settings extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('m_settings');
    }

    function add()
    {
        $data['error'] = NULL;
        if($this->input->post())
        {
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'trim|required|min_length[1]|max_length[200]|is_unique[settings.name]'
                ),
                array(
                    'field' => 'value',
                    'label' => 'Isi',
                    'rules' => 'trim|required',
                )
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            if($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
                $this->session->set_flashdata('msg', validation_errors());
                redirect(base_url().'index.php/admin/setting_add/');
            }
            else 
            {
                $data = array(
                    'name' => $this->input->post('name'),
                    'value' => $this->input->post('value')
                );
                $user_id = $this->m_settings->create($data);
                redirect(base_url().'index.php/admin/setting');
            }
            
        }

        
    }







    function edit()
    {
        $data['error'] = NULL;
        if($this->input->post())
        {
            $config = array(
                array(
                    'field' => 'id',
                    'label' => 'id',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'trim|required|min_length[1]|max_length[200]'
                ),
                array(
                    'field' => 'value',
                    'label' => 'Isi',
                    'rules' => 'trim|required',
                )
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $id =  $this->input->post('id');
            if($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
                $this->session->set_flashdata('msg', validation_errors());
                redirect(base_url().'index.php/admin/setting_edit/'.$id);
            }
            else 
            {
                $data = array(
                    'name' => $this->input->post('name'),
                    'value' => $this->input->post('value')
                );
                
                $user_id = $this->m_settings->update($id, $data);
                redirect(base_url().'index.php/admin/setting');
            }
            
        }

      
    }







}