<div class="main-content">
            <!-- Breadcrumbs Start -->
            <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="<?= base_url()?>assets/images/banner/home1.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title">My Account</h1>

                </div>
            </div>
            <!-- Breadcrumbs End -->            

    		<!-- My Account Section Start -->
    		<div class="rs-login pt-100 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                    <div class="noticed">
                        <div class="main-part">                           
                            <div class="method-account">
                                <h2 class="login">Login</h2>
                                <?php
                                if($error)
                                {
                                    echo '<div style="color:red" >Hmm, we don\'t recognize you. Please try again.</div><br>';
                                }?>
                                <form action="<?=  base_url()?>users/login/" method="post">
                                    <input type="text" name="username" placeholder="Username" required="">
                                    <input type="password" name="password" placeholder="Password" required="">
                                    <button type="submit" class="readon submit-btn">login</button>
                                    <div class="last-password">
                                        <p>Forgot your password? <a href="#">reset password</a></p>
                                        <!-- <p>Not registered? <a href="#">Create an account</a></p> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- My Account Section End -->  

        </div> 