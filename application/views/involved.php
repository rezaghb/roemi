<!-- Main content Start -->
<div class="main-content" style="background: #fff">
    <!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        <div class="breadcrumbs-img">
            <img src="<?= base_url()?>assets/images/banner/home1.jpg" alt="Breadcrumbs Image">
        </div>
        <div class="breadcrumbs-text white-color">
            <h1 class="page-title">Involved</h1>
            <ul>
                <!-- <li>
                    <a class="active" href="index.html">Home</a>
                </li> -->
                <!-- <li>About Us</li> -->
            </ul>
        </div>
    </div>
    <!-- Breadcrumbs End -->
    <!-- Counter Section Start -->
    <div id="rs-about" class="rs-about style3 pt-50 md-pt-30">
        <div class="container">
            <div class="row y-middle">
                <div class="col-lg-4 lg-pr-0 md-mb-30">
                <div class="container">
                    <div class="row y-middle">
                        <div class="sec-title mb-50 md-mb-30">
                            <div class="about-intro">
                                <div class="sec-title">
                                    <div class="sub-title orange">Join Mambers Now</div>
                                </div>
                                <div style="display:flex">
                                    <div style="display:flex">
                                        <img class="image-icon" src="<?= base_url()?>assets/images/phone.png">
                                    </div>
                                    <span class="download"><a href="https://www.apple.com/id/app-store/"> IOS
                                                Download</a></span>
                                    <div style="display:flex">
                                        <img class="image-icon" src="<?= base_url()?>assets/images/phone.png">
                                    </div>
                                    <span class="download"><a
                                                href="http://play.google.com/store/apps/details?id=com.google.android.apps.maps">
                                                Android Download</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- <div class="about-intro">
                        <div class="sec-title">
                            <div class="sub-title orange">Donasi</div>
                        </div>
                    </div> -->
                </div>
                <div class="col-lg-8 pl-83 md-pl-15">
                    <div class="row rs-counter couter-area">
                        <div class="col-md-4 sm-mb-30">
                            <div class="sec-title">
                                <div class="sub-title orange">Lembaga</div>
                            </div>
                            <div class="counter-item one">
                                <a href="<?=base_url("index.php/pages/donasi_lembaga")?>" role="button">
                                    <span>
                                        <span>
                                            <i class="fa fa-donation"></i>
                                            <span> Donasi Lembaga</span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-4 sm-mb-30">
                            <div class="sec-title">
                                <div class="sub-title orange">Mandiri</div>
                            </div>
                            <div class="counter-item two">
                                <a href="<?=base_url("index.php/pages/donasi_pribadi")?>" role="button">
                                    <span>
                                        <span>
                                        <div class="icon-part">
        							        Donasi Mandiri
        						        </div>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Counter Section End -->
    <!-- <div id="rs-about" class="rs-event home12style style3 pt-100 md-pt-70">
        <div class="container">
            <div class="row y-middle">
                <div class="sec-title mb-50 md-mb-30">
                    <div class="about-intro">
                        <div class="sec-title">
                            <div class="sub-title orange">Join Mambers Now</div>
                        </div>
                        <div style="display:flex">
                            <div style="display:flex">
                                <img class="image-icon" src="<?= base_url()?>assets/images/phone.png">
                            </div>
                            <span class="download"><a href="https://www.apple.com/id/app-store/"> IOS
                                        Download</a></span>
                            <div style="display:flex">
                                <img class="image-icon" src="<?= base_url()?>assets/images/phone.png">
                            </div>
                            <span class="download"><a
                                        href="http://play.google.com/store/apps/details?id=com.google.android.apps.maps">
                                        Android Download</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div id="rs-about" class="rs-about style3 pt-0 md-pt-0" style="margin-bottom: 10px">
        <div class="container">
            <div class="row y-middle"  style="margin-top: 10px">
                <div class="col-lg-4 lg-pr-0 md-mb-30">
                    <div class="about-intro">
                        <div class="sec-title">
                            <div class="sub-title orange">Subscribe on</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 pl-83 md-pl-15">
                    <div class="row rs-counter couter-area">
                        <div class="col-md-4 sm-mb-30">
                            <div class="counter-item one">
                                <a href="https://music.apple.com" role="button">
                                    <span>
                                        <span>
                                            <i class="fa fa-apple"></i>
                                            <span>Apple Mussic</span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 sm-mb-30">
                            <div class="counter-item two">
                                <a href="https://spotify.com" role="button">
                                    <span>
                                        <span>
                                            <i class="fa fa-spotify"></i>
                                            <span>Spotify</span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="counter-item three">
                                <a href="https://youtube.com" role="button">
                                    <span>
                                        <span>
                                            <i class="fa fa-youtube"></i>
                                            <span>Youtube</span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Main content End -->