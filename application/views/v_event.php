<!-- gallery -->
<div class="main-content">
    <!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        <div class="breadcrumbs-img">
              <img src="<?= base_url()?>assets/images/breadcrumbs/4.jpg" alt="Breadcrumbs Image">
        </div>
        <div class="spesies">
            <div class="container white-wrapper">
                <div class="row text-center">
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/foto/'.$this->lang)?>">Foto</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/video/'.$this->lang)?>">Video</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/audio/'.$this->lang)?>">Audio</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/material/'.$this->lang)?>">Material</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/news/'.$this->lang)?>">News</a></h3>
                    </div>
                    <div class="col-md-2 ">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/event/'.$this->lang)?>">Event</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs End -->


    <!-- Events Section Start -->
    <div class="rs-event orange-color pt-100 pb-100 md-pt-70 md-pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-60 col-md-6">
                    <div class="event-item">
                        <div class="event-short">
                            <div class="featured-img">
                                <img src="<?= base_url()?>assets/images/roemi/2.png" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-60 col-md-6">
                    <div class="event-item">
                        <div class="event-short">
                            <div class="featured-img">
                                <img src="<?= base_url()?>assets/images/roemi/5.png" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-60 col-md-6">
                    <div class="event-item">
                        <div class="event-short">
                            <div class="featured-img">
                                <img src="<?= base_url()?>assets/images/roemi/3.png" alt="Image">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="rs-slider main-home">
                    <div class="container">
                        <div class="sec-title mb-50 md-mb-30 text-center">
                            <h2 class="title mb-0">Link Selengkapnya Di Aplikasi Reach</h2>
                        </div>
                        <div class="btn-part btnMore">
                            <a class="readon2 orange" href="#">Link Ios</a>
                        </div>
                        <div class="btn-part btnMore">
                            <a class="readon2 orange" href="#">Link Android</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Events Section End -->
</div>
<!-- end gallery -->