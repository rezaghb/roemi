<div class="main-content">
    <!-- Slider Section Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        <div class="breadcrumbs-img">
            <img src="<?= base_url()?>assets/images/banner/home1.jpg" alt="Breadcrumbs Image">
        </div>
        <div class="breadcrumbs-text white-color">
            <h1 class="page-title">Resource</h1>
            <ul>
                <p></p>
            </ul>
        </div>
    </div>
    <!-- CTA Section Start -->
    <div class="rs-partner pt-150 pb-150 md-pt-90 md-pb-90 gray-bg">
        <div class="partition-bg-wrap home2">
            <div class="container">
                <div class="row y-bottom">
                <?php foreach($resources as  $i => $resource){ ?>
                    <div class="col-lg-6 pb-50 md-pt-100 md-pb-100">
                        <div class="podcast">
                            <div class="video-wrap">
                                <a class="popup-video" href="<?=$resource['content1']?>">
                                    <i class="fa fa-play"></i>
                                    <h4 class="title mb-0" style="color:#fff"><?=$resource['post_title']?></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php }?>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    .popup-video{
        color: #ffffff;
        display: block;
        overflow: hidden;
        /* max-width: 270px; */
        text-align: center;
        padding: 100px 10px;
    }
</style>
