<!-- Main content Start -->
    <div class="main-content">
            <!-- Breadcrumbs Start -->
            <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="<?= base_url()?>assets/images/banner/home1.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color padding">
                    <h1 class="page-title">Get in Touch</h1>
                    <ul>
                        <li>
                            <a class="active" href="index.html">Home</a>
                        </li>
                        <li>Donation Confirm</li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs End -->            

    		<!-- Contact Section Start -->
    		<div class="contact-page-section orange-color pt-100 pb-100 md-pt-70 md-pb-70">
            	<div class="container">
            	

            		<div class="row align-items-end contact-bg1">
            			<div class="col-lg-4 md-pt-50 lg-pr-0">
            				<div class="contact-image">
            					<img src="<?= base_url()?>assets/images/contact/2.png" alt="Contact Images">
            				</div>
            			</div>
            			<div class="col-lg-8 lg-pl-0">
			        		<div class="rs-quick-contact new-style">
                                <div class="inner-part mb-35">
                                    <h2 class="title mb-15">Confirmation</h2>
                                    <p>                                
                                    <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>

                                </p>
                                </div>
                                <div id="form-messages"></div>
                                <form id="contact-formz" method="post" action="<?= base_url('index.php/actionPages/add')?>">
                                    <div class="row">
                                        <div class="col-lg-6 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="hidden" id="name" name="type" value="confirmation">
                                            <input class="from-control" type="text" id="name" name="title_id" placeholder="Name" required="confirmation">

                                            <input id="firstname" class="form-control" name="redirect-success" type="hidden" value="index.php/pages/konfirmasi">
								<input id="firstname" class="form-control" name="redirect-error" type="hidden" value="index.php/pages/konfirmasi">

                                        </div> 
                                        <div class="col-lg-6 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="text" id="email" name="content_id1" placeholder="Bank" required="">
                                        </div>   
                                        <div class="col-lg-6 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="text" id="name" name="content_id2" placeholder="No Rekening" required="">
                                        </div> 
                                        <div class="col-lg-6 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="text" id="email" name="content_id3" placeholder="Jumlah" required="">
                                        </div>   
                                        <div class="col-lg-6 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="date" id="email" name="content_id5" placeholder="Tgl Transfer" required="">
                                        </div>   
                                        <div class="col-lg-6 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="hidden" id="images_en" name="content_id4" placeholder="Bukti Transfer" required="">
                                            <input type="file" name="file" id="file2" class="btn btn-primary"  onchange="uploadFile(2)">
                                        </div>   

                              
                                     
                                    </div>
                                    <div class="form-group mb-0">
                                        <input class="btn-send" type="submit" value="Submit Now">
                                    </div>       
                                </form>
                            </div> 
            			</div>
            		</div>
            	</div>
            </div>
            <!-- Contact Section End -->  


        </div> 
        <!-- Main content End --> 


        
        <script>
              function uploadFile(uid) {
                var files;
                if(uid == 1){
                    files = document.getElementById("file").files;
                }else{
                   files = document.getElementById("file2").files;
                }
                  
                  if(files.length > 0 ){
                      var formData = new FormData();
                      formData.append("file", files[0]);
                      var xhttp = new XMLHttpRequest();
                      // Set POST method and ajax file path
                      xhttp.open("POST", "<?=base_url('index.php/upload/own')?>", true);
                      // call on request changes state
                      xhttp.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                              var response = this.responseText;
                              if(response != 0){
                                if(uid == 1){
                                  document.getElementById("images_id").value = response;
                                }else{
                                  document.getElementById("images_en").value = response;
                                }
                              }else{
                                alert("File not uploaded.");
                              }
                          }
                      };
                      // Send request with data
                      xhttp.send(formData);
                  }else{
                      alert("Please select a file");
                  }
              }
            </script>