		<!-- Main content Start -->
		<div class="main-content">
		    <!-- Main content Start -->
		    <div class="main-content">
		        <div class="main-content">
		            <!-- Events Section Start -->
		            <div class="sec-title3 text-center mb-50">
		                <h2 class="title"> KOLABORATOR</h2>
		            </div>
		            <div class="rs-gallery pt-100 pb-100 md-pt-70 md-pb-70">
		                <div class="container">
		                    <div class="row">
		                        <?php foreach($partners as  $i => $partner){ ?>
		                        <div class="col-lg-4 mb-30 col-md-6">
		                            <div class="gallery-item">
		                                <div class="gallery-img">
		                                    <a class="image-popup" href="#"><img
		                                            src="<?= base_url("uploads/".$partner['post_title'])?>"
		                                            alt="<?=$partner['content1']?>"></a>
		                                </div>
		                                <div class="title">
		                                    Group Study Time
		                                </div>
		                            </div>
		                        </div>
		                        <?php }?>
		                    </div>
		                </div>
		            </div>
		            <!-- Events Section End -->
		        </div>
		    </div>
		    <!-- Main content End -->

		    <!-- Banner Section End -->
		</div>
		<!-- Main content End -->