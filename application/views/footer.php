       <?php 
       $logo = $this->m_settings->getByName("logo");
       $address = $this->m_settings->getByName("address");
       $telp = $this->m_settings->getByName("telp");
       $email = $this->m_settings->getByName("email");
       $fb = $this->m_settings->getByName("fb");
       $tw = $this->m_settings->getByName("twitter");
       $ig = $this->m_settings->getByName("ig");
       ?>
       
       
       <!-- Footer Start -->
       <footer id="rs-footer" class="rs-footer home9-style">
            <div class="footer-bottom">
                <div class="container">                    
                    <div class="row y-middle">
                        <div class="col-lg-4 md-mb-20">
                            <!-- <div class="footer-logo md-text-center">
                                <a href="index.html"><img src="<?= base_url()?>assets/images/roemi-logo.png" alt=""></a>
                            </div> -->
                        </div>
                        <div class="col-lg-8 md-mb-20">
                            <!-- <div class="copyright text-center md-text-start">
                                <p>&copy; <?=date('Y')?> Build With Love Roemi. </p>
                            </div> -->
                            <ul class="footer-social">
                                <li><a href="<?=$tw["value"]?>"><i class="fa fa-twitter fa-2x"></i></a></li>
                                <li><a href="<?=$fb["value"]?>"><i class="fa fa-facebook  fa-2x"></i></a></li>
                                <li><a href="<?=$ig["value"]?>"><i class="fa fa-instagram  fa-2x"></i></a></li>
                                <li><a href="<?=$ig["value"]?>"><i class="fa fa-youtube  fa-2x"></i></a></li> 
                                <li><a href="<?=$ig["value"]?>"><i class="fa fa-linkedin  fa-2x"></i></a></li>
                            </ul>

                            
                        </div>
                       
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div id="scrollUp">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- End scrollUp  -->

        <!-- Search Modal Start -->
        <div class="modal fade search-modal" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
            <button type="button" class="close" data-bs-dismiss="modal">
              <span class="flaticon-cross"></span>
            </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here..." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- modernizr js -->
        <script src="<?= base_url()?>assets/js/modernizr-2.8.3.min.js"></script>
        <!-- jquery latest version -->
        <script src="<?= base_url()?>assets/js/jquery.min.js"></script>
        <!-- Bootstrap v5.0.2 js -->
        <script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
        <!-- Menu js -->
        <script src="<?= base_url()?>assets/js/rsmenu-main.js"></script> 
        <!-- op nav js -->
        <script src="<?= base_url()?>assets/js/jquery.nav.js"></script>
        <!-- owl.carousel js -->
        <script src="<?= base_url()?>assets/js/owl.carousel.min.js"></script>
        <!-- Slick js -->
        <script src="<?= base_url()?>assets/js/slick.min.js"></script>
        <!-- isotope.pkgd.min js -->
        <script src="<?= base_url()?>assets/js/isotope.pkgd.min.js"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="<?= base_url()?>assets/js/imagesloaded.pkgd.min.js"></script>
        <!-- wow js -->
        <script src="<?= base_url()?>assets/js/wow.min.js"></script>
        <!-- Skill bar js -->
        <script src="<?= base_url()?>assets/js/skill.bars.jquery.js"></script>
        <script src="<?= base_url()?>assets/js/jquery.counterup.min.js"></script>        
         <!-- counter top js -->
        <script src="<?= base_url()?>assets/js/waypoints.min.js"></script>
        <!-- video js -->
        <script src="<?= base_url()?>assets/js/jquery.mb.YTPlayer.min.js"></script>
        <!-- magnific popup js -->
        <script src="<?= base_url()?>assets/js/jquery.magnific-popup.min.js"></script> 
        <!-- parallax-effect js -->
        <script src="<?= base_url()?>assets/js/parallax-effect.min.js"></script>     
        <!-- plugins js -->
        <script src="<?= base_url()?>assets/js/plugins.js"></script>
        <!-- contact form js -->
        <script src="<?= base_url()?>assets/js/contact.form.js"></script>
        <!-- main js -->
        <script src="<?= base_url()?>assets/js/main.js"></script>
    </body>
</html>