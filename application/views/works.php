<div class="main-content">



<div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="<?= base_url()?>assets/images/banner/home1.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title">Our Works</h1>
                    <ul>
                        <p>
                            <!-- <img src="<?= base_url()?>assets/images/logo.png" style="width: 200px"> -->
                    </p>
                    </ul>
                </div>
            </div>



            <!-- About Section Start -->
            <div id="rs-about" class="rs-about style1 pb-100 md-pb-70" style="background: #fff">
                <div class="container">
                    <div class="row">
              
                        <div class="col-lg-6 pr-50 md-pr-15">
                            <div class="about-part">
                                <div class="sec-title mb-40">
                                    <div class="sub-title primary wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms"><?=$page['post_title']?></div>
                                    <div class="desc wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms" >
                                    <?=$page['content1']?>
                                    </div>
                                    <br>
                                    <div class="banner-btn wow fadeInUp" data-wow-delay="1500ms" data-wow-duration="2000ms">
                                        <a class="readon banner-style" href="<?=$page['content2']?>">Go to Koper</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6 pr-50 md-pr-15">
                            <div class="about-part">
                                <div class="sec-title mb-40">
                                    <div class="sub-title primary wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms"><?=$reach['post_title']?></div>
                                    <div class="desc wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                                    <?=$reach['content1']?>
                                    </div>
                                    <br>
                                    <div class="banner-btn wow fadeInUp" data-wow-delay="1500ms" data-wow-duration="2000ms">
                                        <a class="readon banner-style" href="<?=$reach['content2']?>">Go to Reach</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Section End -->
            <!-- list news -->
            <div class="rs-inner-blog orange-color pt-100 pb-100 md-pt-70 md-pb-70" style="background: #fff">
              <div class="container">
                  <div class="row">
   
                      <div class="col-lg-8 pr-50 md-pr-15">
                          <div class="row">


                              <?php foreach($news as  $i => $new){ ?>
                              <div class="col-lg-12 mb-70">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="#"><img src="assets/images/blog/inner/2.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="#"><?=$new['post_title']?></a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> <?=$new['date_added']?>                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   

                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                            <?=$new['content1']?>                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <?php } ?>

                          </div>
                      </div>
                  </div> 
              </div>
            </div>
            <!-- list news end -->
        </div> 