<!-- gallery -->
<div class="main-content margin-custome">
    <!-- Events Section Start -->
    <div class="sec-title3 text-center mb-30">
        <h2 class="title"> INISIATIF</h2>
    </div>
    <div class="rs-gallery pt-20 pb-20 md-pt-20 md-pb-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-30 col-md-6">
                    <div class="gallery-img">
                        <a href="<?= base_url('pages/reach/'.$this->lang)?>"><img
                                src="<?= base_url()?>assets/images/inisiatif/reach.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6 mb-30 col-md-6">
                    <div class="gallery-img">
                        <a href="<?= base_url('pages/koper/'.$this->lang)?>"><img
                                src="<?= base_url()?>assets/images/inisiatif/kopper.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Events Section End -->
</div>
<!-- end gallery -->