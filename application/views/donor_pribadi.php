


        <div class="main-content">
        <div class="rs-breadcrumbs breadcrumbs-overlay">
					<div class="breadcrumbs-img">
							<img src="<?=base_url()?>assets/images/breadcrumbs/2.jpg" alt="Breadcrumbs Image">
					</div>
					<div class="breadcrumbs-text white-color">
							<h1 class="page-title">Donasi Pribadi</h1>
							<!-- <ul>
								<li>
									<a class="active" href="index.html">Home</a>
								</li>
								<li>About Us</li>
							</ul> -->
					</div>
			</div>

            <!-- About Section Start -->
            <div id="rs-about" class="rs-about style1 pb-100 md-pb-70" style="background: #fff" >
                <div class="container">
                    <div class="row">
              
                        <div class="col-lg-6 pr-50 md-pr-15">
                            <div class="about-part">
                                <div class="sec-title mb-40">
                                    <div class="sub-title primary wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms"><?=$page['post_title']?></div>
                                    <div class="desc wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms" >
                                
                                        Bank <?=$page['content3']?><br>
                                        Atas Nama <?=$page['content2']?><br>
                                        Nomor Reking <?=$page['content1']?>
                                    </div>
                                    <br>
                                    <div class="banner-btn wow fadeInUp" data-wow-delay="1500ms" data-wow-duration="2000ms">
                                        <a class="readon banner-style" href='<?=base_url("index.php/pages/konfirmasi")?>'>Konfirmasi</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6 pr-50 md-pr-15">
                            <div class="about-part">
                                <div class="sec-title mb-40">
                                    <div class="sub-title primary wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">Scan For Donation</div>
                                    <div class="desc wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                                    <img class="image-icon" src="<?= base_url('uploads/'.$qr['content1'])?>">
                                    </div>
                                    <br>
                                    <div class="banner-btn wow fadeInUp" data-wow-delay="1500ms" data-wow-duration="2000ms">
                                        <a class="readon banner-style" href='<?=base_url("index.php/pages/konfirmasi")?>'>Konfirmasi</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
 
        </div> 