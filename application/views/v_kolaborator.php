<!-- gallery -->
<div class="main-content  margin-custome">
    <!-- Events Section Start -->

    <div class="sec-title3 text-center mt-20 mb-10">
        <h2 class="title"> KOLABORATOR</h2>
    </div>
    <div class="rs-gallery pt-100 pb-100 md-pt-70 md-pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-30 col-md-6">
                    <div class="gallery-img">
                        <a class="image-popup" href="assets/images/gallery/1.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 mb-30 col-md-6">
                    <div class="gallery-img">
                        <a class="image-popup" href="assets/images/gallery/2.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/2.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 mb-30 col-md-6">
                    <div class="gallery-img">
                        <a class="image-popup" href="assets/images/gallery/3.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/3.jpg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Events Section End -->
</div>
<!-- end gallery -->