<!-- gallery -->
<div class="main-content">
    <!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        <div class="breadcrumbs-img">
            <img src="<?= base_url()?>assets/images/roemi/1.png" alt="Breadcrumbs Image">
        </div>
        <div class="spesies">
            <div class="container white-wrapper">
                <div class="row text-center">
                    <div class="col-md-4 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/fans/'.$this->lang)?>">Jadi Pendukung</a></h3>
                    </div>
                    <div class="col-md-4 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/joinus/'.$this->lang)?>">Jadi Anggota</a></h3>
                    </div>
                    <div class="col-md-4">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/investor/'.$this->lang)?>">Jadi Investor</a></h3>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs End -->
    <br>
    <!-- Events Section Start -->
    <div class="rs-slider main-home">
        <div class="container">
            <div class="sec-title mb-50 mt-20 md-mb-30 text-center">
                <h2 class="title mb-0">Follow dan subscribe ROEMI jadilah komunitas penggerak perubahan utuk dunia yang adil dan setara</h2>
            </div>
            <div class="btn-part btnMore1">
                <a class="readon2 orange" href="#">Link Spotify</a>
            </div>
        </div>
    </div>

    <!-- Events Section End -->
</div>
<!-- end gallery -->