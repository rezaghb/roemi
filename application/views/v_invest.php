<?php 
       $logo = $this->m_settings->getByName("logo");
       $address = $this->m_settings->getByName("address");
       $telp = $this->m_settings->getByName("telp");
       $email = $this->m_settings->getByName("email");
       $fb = $this->m_settings->getByName("fb");
       $tw = $this->m_settings->getByName("twitter");
       $ig = $this->m_settings->getByName("ig");
       ?>

<!-- gallery -->
<div class="main-content">
    <!-- Breadcrumbs Start -->
    
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        <div class="breadcrumbs-img">
            <img src="<?= base_url()?>assets/images/roemi/1.png" alt="Breadcrumbs Image">
        </div>
        <div class="spesies">
            <div class="container white-wrapper">
                <div class="row text-center">
                    <div class="col-md-4 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/fans/'.$this->lang)?>">Jadi Pendukung</a></h3>
                    </div>
                    <div class="col-md-4 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/joinus/'.$this->lang)?>">Jadi Anggota</a></h3>
                    </div>
                    <div class="col-md-4">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/investor/'.$this->lang)?>">Jadi Investor</a></h3>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs End -->
    <br>
    <!-- Events Section Start -->
    <div class="rs-slider main-home">
        <div class="container">
            <div class="sec-title mb-50 md-mb-30 text-center">
                <h2 class="title mb-0">Berinvestasilah untuk dunia yang adil dan setara. Bersama ROEMI membangun ekosistem perubahan sosial yang Vibrant di Idonesia dan dunia</h2>
            </div>
            <li class="btn-part">
                <a class="apply-btn" href="#">Link Download PDF investor</a>
            </li>
            <li class="btn-part">
                <a class="apply-btn" href="#">Link Download PDF investor Individu</a>
            </li>
        </div>
    </div>
    <!-- Events Section End -->

    <div class="rs-slider main-home">
        <div class="container">
            <div class="btn-part mt-10 mb-10">
                <h3>Silahkan Menghubungi Kami</h3>
            </div>
        </div>
    </div>

        <div class="contact-page-section pt-20 pb-20 md-pt-20 md-pb-20">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 pl-60 md-pl-15">
                        <div class="contact-address-section style2">
                            <div class="contact-info mb-15 md-mb-30">
                                <div class="icon-part">
                                    <i class="fa fa-home"></i>
                                </div>
                                <div class="content-part">
                                    <h5 class="info-subtitle">Address</h5>
                                    <h4 class="info-title"><?=$address['value']?></h4>
                                </div>
                            </div>
                            <div class="contact-info mb-15 md-mb-30">
                                <div class="icon-part">
                                    <i class="fa fa-envelope-open-o"></i>
                                </div>
                                <div class="content-part">
                                    <h5 class="info-subtitle">Email Address</h5>
                                    <h4 class="info-title"><a href="mailto:hello@roemi.id"><?=$email['value']?></a></h4>
                                </div>
                            </div>
                            <div class="contact-info">
                                <div class="icon-part">
                                    <i class="fa fa-headphones"></i>
                                </div>
                                <div class="content-part">
                                    <h5 class="info-subtitle">Phone Number</h5>
                                    <h4 class="info-title"><a href="tel+0885898745"><?=$telp['value']?></a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pl-60 md-pl-15">
                        <div class="contact-comment-box">
                            <div id="form-messages"></div>
                            <form id="contact-form" method="post" action="<?= base_url('contact/add')?>">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-6 mb-35 col-md-6 col-sm-6">
                                            <input class="from-control" type="text" id="name" name="name"
                                                placeholder="Name" required="">
                                        </div>
                                        <div class="col-lg-6 mb-35 col-md-6 col-sm-6">
                                            <input class="from-control" type="text" id="email" name="email"
                                                placeholder="Email" required="">
                                        </div>
                                        <div class="col-lg-12 mb-35 col-md-12 col-sm-12">
                                            <input class="from-control" type="text" id="subject" name="subject"
                                                placeholder="Subject" required="">
                                        </div>
                                        <div class="col-lg-12 mb-50">
                                            <textarea class="from-control" id="message" name="message"
                                                placeholder=" Message" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <input class="btn-send" type="submit" value="Send">
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end gallery -->