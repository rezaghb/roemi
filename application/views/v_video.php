<!-- gallery -->
<div class="main-content">
    <!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        <div class="breadcrumbs-img">
            <img src="<?= base_url()?>assets/images/breadcrumbs/4.jpg" alt="Breadcrumbs Image">
        </div>
        <div class="spesies">
            <div class="container white-wrapper">
                <div class="row text-center">
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/foto/'.$this->lang)?>">Foto</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/video/'.$this->lang)?>">Video</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/audio/'.$this->lang)?>">Audio</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/material/'.$this->lang)?>">Material</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/news/'.$this->lang)?>">News</a></h3>
                    </div>
                    <div class="col-md-2 ">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/event/'.$this->lang)?>">Event</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs End -->

    <!-- Events Section Start -->
    <!-- <div class="rs-partner pt-150 pb-150 md-pt-90 md-pb-90 gray-bg">
        <div class="partition-bg-wrap home2">
            <div class="container">
                <div class="row y-bottom">
                    <?php foreach($resources as  $i => $resource){ ?>
                    <div class="col-lg-6 pb-50 md-pt-100 md-pb-100">
                        <div class="podcast">
                            <div class="video-wrap">
                                <a class="popup-video" href="<?=$resource['content1']?>">
                                    <i class="fa fa-play"></i>
                                    <h4 class="title mb-0" style="color:#fff"><?=$resource['post_title']?></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php }?>

                </div>
            </div>
        </div>
    </div> -->
    <!-- Events Section End -->
    <div class="rs-partner pt-50 pb-50 md-pt-50 md-pb-50 gray-bg">
        <div class="partition-bg-wrap home2">
            <div class="container">
                <div class="row y-bottom">
                <?php foreach($resources as  $i => $resource){ ?>
                    <div class="col-lg-6 pb-50 md-pt-100 md-pb-100">
                        <div class="podcast">
                            <div class="video-wrap">
                                <a class="popup-video" href="<?=$resource['content1']?>" style="background:url(<?= base_url()?>assets/images/roemi/3.png)">
                                    <i class="fa fa-play"></i>
                                    <h4 class="title mb-0" style="color:#fff"><?=$resource['post_title']?></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php }?>

                </div>
            </div>
        </div>
    </div>



</div>
<!-- end gallery -->
<style>
    .popup-video{
        color: #ffffff;
        display: block;
        overflow: hidden;
        /* max-width: 270px; */
        text-align: center;
        padding: 100px 10px;
    }
</style>
