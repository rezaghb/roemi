      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Users</h4> 
              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_user_add/")?>">Add</a>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Email</th>
                            <th>UserName</th>
                            <!-- <th>Actions</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($users as  $i => $user){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><?=$user['email']?></td>
                            <td><?=$user['username']?></td>
                            <!-- <td>
                              <a class="btn btn-outline-primary" href="<?=base_url("admin/user_edit/".$user['user_id'])?>">Edit</a>
                            </td> -->
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->