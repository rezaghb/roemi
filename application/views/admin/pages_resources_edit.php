<div class="col-md-12 col-xl-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Data</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>
                                

                                <!-- <ul class="nav nav-tabs" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Bahasa</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile-1" role="tab" aria-controls="profile-1" aria-selected="false">English</a>
                                  </li>
                                </ul> -->


                                <div class="tab-content">
                                  <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="mediax">
                                      
                                      <div class="media-body">

                                      <form class="cmxform" id="editSettingForm" method="post" action="<?=base_url("actionPages/edit")?>">
                                 
                                        <div class="form-group">
                                            <label for="firstname">Sosmed</label>
                                            
                                            <input id="firstname" class="form-control" name="id-id" type="hidden" value="<?=$pageid['post_id']?>">

                                            <input id="firstname" class="form-control" name="id-en" type="hidden" value="<?=$pageen['post_id']?>">


                                            <input id="firstname" class="form-control" name="type" type="hidden" value="resources-sosmed">
                                            <select class="form-control" name="title_id">
                                            
                                              <option value="facebook"  <?php if($pageid['post_title'] == 'facebook'){echo "selected"; }?>>facebook</option>
                                              <option value="instagram" <?php if($pageid['post_title'] == 'instagram'){echo "selected"; }?>>instagram</option>
                                              <option value="twitter" <?php if($pageid['post_title'] == 'twitter'){echo "selected"; }?>>twitter</option>
                                              <option value="youtube" <?php if($pageid['post_title'] == 'youtube'){echo "selected"; }?>>youtube</option>
                                            </select>
                                            
                                            <input id="firstname" class="form-control" name="redirect-success" type="hidden" value="admin/pages_resources/ece73802ab509a658a8ef3a54fdfe068">
                                            <input id="firstname" class="form-control" name="redirect-error" type="hidden" value="admin/pages_resources_edit/<?=$idx?>">

                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Link</label>
                                            <input id="lastname" class="form-control" name="content_id1" type="text" value="<?=$pageid['content1']?>">
                                        </div>
                                        
                                      </div> <!--media-body-->
                                    </div>
                                  </div>
                                  <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="mediax">
                                      <div class="media-body">

                                     
                                        <div class="form-group">
                                            <label for="firstname">Sosmed</label>
                                            <select class="form-control" name="title_en">
                                            <option value="facebook"  <?php if($pageen['post_title'] == 'facebook'){echo "selected"; }?>>facebook</option>
                                              <option value="instagram" <?php if($pageen['post_title'] == 'instagram'){echo "selected"; }?>>instagram</option>
                                              <option value="twitter" <?php if($pageen['post_title'] == 'twitter'){echo "selected"; }?>>twitter</option>
                                              <option value="youtube" <?php if($pageen['post_title'] == 'youtube'){echo "selected"; }?>>youtube</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Link</label>
                                            <input id="lastname" class="form-control" name="content_en1" type="text" value="<?=$pageen['content1']?>">
                                        </div>
                          


                                      </div> <!--media-body-->
                                    </div>
                                  </div>

                                  <input class="btn btn-primary" type="submit" value="Submit">
                                    
                                    </form>

                                </div>


       


                                </div>
                            </div>
                            </div>
                        </div>



                </div>
              </div>
            </div>