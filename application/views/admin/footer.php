 
        <!-- partial:partials/_footer.html -->
        <!-- <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © <?=date('Y')?>.  Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="ti-heart text-danger ms-1"></i></span>
          </div>
        </footer> -->
        <!-- partial -->



      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?= base_url()?>statics/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="<?= base_url()?>statics/vendors/chart.js/Chart.min.js"></script>
  <script src="<?= base_url()?>statics/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="<?= base_url()?>statics/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="<?= base_url()?>statics/js/dataTables.select.min.js"></script>
  <script src="<?= base_url()?>statics/vendors/jquery-validation/jquery.validate.min.js"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="<?= base_url()?>statics/js/off-canvas.js"></script>
  <script src="<?= base_url()?>statics/js/hoverable-collapse.js"></script>
  <script src="<?= base_url()?>statics/js/template.js"></script>
  <script src="<?= base_url()?>statics/js/settings.js"></script>
  <script src="<?= base_url()?>statics/js/todolist.js"></script>
  <script src="<?= base_url()?>statics/js/form-validation.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?= base_url()?>statics/js/jquery.cookie.js" type="text/javascript"></script>
  <script src="<?= base_url()?>statics/js/dashboard.js"></script>
  <script src="<?= base_url()?>statics/js/Chart.roundedBarCharts.js"></script>

  <!-- End custom js for this page-->
</body>

</html>

