<div class="col-lg-12 ">



      <!-- partial -->
      <div class="main-panelx">
        <div class="content-wrapper">
          <div class="card">
          <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>

            <div class="card-body">
              <h4 class="card-title">Partners List</h4> 
              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_partners_sosmed/")?>">Add</a>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Logo</th>
                            <th>Name</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($pages as  $i => $page){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><img src="<?=base_url('uploads/'.$page['post_title'])?>" width="100"></td>
                            <td><?=$page['content1']?></td>
                            <td>
                              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_partners_edit/".$page['container'])?>">Edit</a>
                            </td>
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->




            </div>