     <?php
         function pages_event_countdown($startdate)
         {
             $date = strtotime($startdate);
             $remaining = $date - time();
     
             $days_remaining = floor($remaining / 86400);
             $hours_remaining = floor(($remaining % 86400) / 3600);
             echo "There are $days_remaining days and $hours_remaining hours left";
         }
     
     
     
     
     ?>
     
     
     
     <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Events</h4> 
              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_event_add/")?>">Add</a>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Time remaining</th>
                            <th>date</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($pages as  $i => $page){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><?=$page['post_title']?></td>
                            <td><?php pages_event_countdown($page['content1'])?></td>
                            <td><?=$page['content1']?> - <?=$page['content2']?></td>
                            <td>
                              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_event_edit/".$page['container'])?>">Edit</a>
                            </td>
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->

