<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Back Office</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?= base_url()?>statics/vendors/feather/feather.css">
  <link rel="stylesheet" href="<?= base_url()?>statics/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="<?= base_url()?>statics/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="<?= base_url()?>statics/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="<?= base_url()?>statics/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url()?>statics/js/select.dataTables.min.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?= base_url()?>statics/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?= base_url()?>statics/statics/images/favicon.png" />

  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>

</head>
<body>

    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
        <a class="navbar-brand brand-logo me-5" href="<?= base_url()?>"><img src="<?= base_url()?>assets/images/logo.png" class="me-2" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="<?= base_url()?>"><img src="<?= base_url()?>assets/images/load.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button>
        <ul class="navbar-nav navbar-nav-right">
          <!-- <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-bs-toggle="dropdown">
              <i class="icon-bell mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="ti-info-alt mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal">Application Error</h6>
                  <p class="font-weight-light small-text mb-0 text-muted">
                    Just now
                  </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="ti-settings mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal">Settings</h6>
                  <p class="font-weight-light small-text mb-0 text-muted">
                    Private message
                  </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="ti-user mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal">New user registration</h6>
                  <p class="font-weight-light small-text mb-0 text-muted">
                    2 days ago
                  </p>
                </div>
              </a>
            </div>
          </li> -->
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" id="profileDropdown">
              <img src="<?= base_url()?>statics/images/faces/face28.jpg" alt="profile"/>
            </a>
            <!-- <ul class="subMenu"> -->
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown subMenu" aria-labelledby="profileDropdown">
                <a class="dropdown-item" href="<?= base_url()?>admin/setting">
                  <i class="ti-settings text-primary"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="<?= base_url()?>users/logout">
                  <i class="ti-power-off text-primary"></i>
                  Logout
                </a>
              </div>
            <!-- </ul> -->
          </li>

        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav> 
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->

      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
        <ul class="nav nav-tabs border-top" id="setting-panel" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="todo-tab" data-bs-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="chats-tab" data-bs-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
          </li>
        </ul>
        <div class="tab-content" id="setting-content">
          <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
            <div class="add-items d-flex px-3 mb-0">
              <form class="form w-100">
                <div class="form-group d-flex">
                  <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                  <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add</button>
                </div>
              </form>
            </div>
            <div class="list-wrapper px-3">
              <ul class="d-flex flex-column-reverse todo-list">
                <li>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="checkbox" type="checkbox">
                      Team review meeting at 3.00 PM
                    </label>
                  </div>
                  <i class="remove ti-close"></i>
                </li>
                <li>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="checkbox" type="checkbox">
                      Prepare for presentation
                    </label>
                  </div>
                  <i class="remove ti-close"></i>
                </li>
                <li>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="checkbox" type="checkbox">
                      Resolve all the low priority tickets due today
                    </label>
                  </div>
                  <i class="remove ti-close"></i>
                </li>
                <li class="completed">
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="checkbox" type="checkbox" checked>
                      Schedule meeting for next week
                    </label>
                  </div>
                  <i class="remove ti-close"></i>
                </li>
                <li class="completed">
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="checkbox" type="checkbox" checked>
                      Project review
                    </label>
                  </div>
                  <i class="remove ti-close"></i>
                </li>
              </ul>
            </div>
            <h4 class="px-3 text-muted mt-5 font-weight-light mb-0">Events</h4>
            <div class="events pt-4 px-3">
              <div class="wrapper d-flex mb-2">
                <i class="ti-control-record text-primary me-2"></i>
                <span>Feb 11 2018</span>
              </div>
              <p class="mb-0 font-weight-thin text-gray">Creating component page build a js</p>
              <p class="text-gray mb-0">The total number of sessions</p>
            </div>
            <div class="events pt-4 px-3">
              <div class="wrapper d-flex mb-2">
                <i class="ti-control-record text-primary me-2"></i>
                <span>Feb 7 2018</span>
              </div>
              <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
              <p class="text-gray mb-0 ">Call Sarah Graves</p>
            </div>
          </div>
          <!-- To do section tab ends -->
          <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
            <div class="d-flex align-items-center justify-content-between border-bottom">
              <p class="settings-heading border-top-0 mb-3 ps-3 pt-0 border-bottom-0 pb-0">Friends</p>
              <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">See All</small>
            </div>
            <ul class="chat-list">
              <li class="list active">
                <div class="profile"><img src="<?= base_url()?>statics/images/faces/face1.jpg" alt="image"><span class="online"></span></div>
                <div class="info">
                  <p>Thomas Douglas</p>
                  <p>Available</p>
                </div>
                <small class="text-muted my-auto">19 min</small>
              </li>
              <li class="list">
                <div class="profile"><img src="<?= base_url()?>statics/images/faces/face2.jpg" alt="image"><span class="offline"></span></div>
                <div class="info">
                  <div class="wrapper d-flex">
                    <p>Catherine</p>
                  </div>
                  <p>Away</p>
                </div>
                <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                <small class="text-muted my-auto">23 min</small>
              </li>
              <li class="list">
                <div class="profile"><img src="<?= base_url()?>statics/images/faces/face3.jpg" alt="image"><span class="online"></span></div>
                <div class="info">
                  <p>Daniel Russell</p>
                  <p>Available</p>
                </div>
                <small class="text-muted my-auto">14 min</small>
              </li>
              <li class="list">
                <div class="profile"><img src="<?= base_url()?>statics/images/faces/face4.jpg" alt="image"><span class="offline"></span></div>
                <div class="info">
                  <p>James Richardson</p>
                  <p>Away</p>
                </div>
                <small class="text-muted my-auto">2 min</small>
              </li>
              <li class="list">
                <div class="profile"><img src="<?= base_url()?>statics/images/faces/face5.jpg" alt="image"><span class="online"></span></div>
                <div class="info">
                  <p>Madeline Kennedy</p>
                  <p>Available</p>
                </div>
                <small class="text-muted my-auto">5 min</small>
              </li>
              <li class="list">
                <div class="profile"><img src="<?= base_url()?>statics/images/faces/face6.jpg" alt="image"><span class="online"></span></div>
                <div class="info">
                  <p>Sarah Graves</p>
                  <p>Available</p>
                </div>
                <small class="text-muted my-auto">47 min</small>
              </li>
            </ul>
          </div>
          <!-- chat tab ends -->
        </div>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("admin/dashboard")?>">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-elements">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="<?= base_url("admin/pages_home/b991d57f9e35813ca5b90dab68b5f276")?>">HOME</a></li>                
                <li class="nav-item"><a class="nav-link" href="<?= base_url("admin/pages_about/87dc9560c7a13615a38eed3e426a8418")?>">ABOUT</a></li>                
                <!-- <ul class="nav-item"><a class="nav-link" href="<?= base_url("admin/pages_works/dfdfe14c07f74c4d98c4ef4f5a556dd9")?>">WORKS</a> -->
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="<?= base_url("admin/pages_works/dfdfe14c07f74c4d98c4ef4f5a556dd9")?>" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                WORKS
                </a>
                <ul class="subMenu">
                  <li><a class="dropdown-item" href="<?= base_url("admin/pages_works_koper/87a00519bc3bcbf74989854fe3ff09da")?>">Koper</a></li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="<?= base_url("admin/pages_works_koper/87a00519bc3bcbf74989854fe3ff09da")?>" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Reach
                    </a>
                      <ul class="subMenu">
                      <li><a class="nav-link" href="<?= base_url("admin/pages_works_reach/514d8f76d8370fafa68dffc984ad85a9")?>">Deskripsi</a></li>
                      <li><a class="nav-link" href="<?= base_url("admin/pages_works_reach_news/")?>">News</a></li>
                      </ul>
                    </li>
                </ul>
              </li>
               
              <li class="nav-item"><a class="nav-link" href="<?= base_url("admin/pages_resources/ece73802ab509a658a8ef3a54fdfe068")?>">RESOURCES</a></li>                
              <li class="nav-item"><a class="nav-link" href="<?= base_url("admin/pages_partners/e15f1bf8049a42b4785ff9c3e7f53c86")?>">PARTNERS</a></li>                
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                INVOLVED
                </a>
                  <ul class="subMenu">
                  <li><a class="nav-link" href="<?= base_url("admin/pages_member/cb6e4c192250e0cd20bc7dfbc94b1f6e")?>">Member</a></li>
                  <li><a class="nav-link" href="<?= base_url("admin/pages_subscribe/84d9015ed888682904e2a60aacb55cce")?>">Subscribe</a></li>
                  <li><a class="nav-link" href="<?= base_url("admin/pages_pribadi/0ce90809ee7460fcce92fcbf3ac263d2")?>">Donor Pribadi</a></li>
                  <li><a class="nav-link" href="<?= base_url("admin/pages_lembaga/99576a8a1652c9bcc9930e2d7d385ead")?>">Donor Lembaga</a></li>
                  </ul>
                </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  GET IN TOUCH
                </a>
                <ul class="subMenu">
                  <li><a class="nav-link" href="<?= base_url("admin/pages_getin_sosmed/16975d4fc3dd4c858cf9a3c3dfc2ba92")?>">Sosial Media</a></li>
                  <li><a class="nav-link" href="<?= base_url("admin/pages_faq/349d8a8ed0ab9f2aeb9e1f76ed4111e3")?>">FAQ</a></li>
                </ul>
              </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("admin/pages_media/fd3a0cdd31b1e03a7540c5ee766ac385")?>">
              <i class="icon-image menu-icon"></i>
              <span class="menu-title">Media</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("admin/contact")?>">
              <i class="icon-open menu-icon"></i>
              <span class="menu-title">Feedback</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("admin/pages_event/d962c3ca56c3ac25725bb76882845e3d")?>">
              <i class="icon-esc menu-icon"></i>
              <span class="menu-title">Event</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("admin/confirmation")?>">
              <i class="icon-contract menu-icon"></i>
              <span class="menu-title">Confirmation</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("admin/user")?>">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Users</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("admin/setting")?>">
              <i class="icon-cog menu-icon"></i>
              <span class="menu-title">Settings</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url()?>backoffice/logout">
              <i class="icon-esc menu-icon"></i>
              <span class="menu-title">Logout</span>
            </a>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      

    <!-- dropdown -->
    <script>
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
    if (!$(this).next().hasClass('show')) {
      $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
    }
    var $subMenu = $(this).next('.dropdown-menu');
    $subMenu.toggleClass('show');

    // var $profil = $(this).next('.dropdown-menu');
    // $profil.toggleClass('show');
  
  
    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
      $('.dropdown-submenu .show').removeClass('show');
    });

    // $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    //   $('.dropdown-submenu .show').removeClass('show');
    // });
  
  
    return false;
  });
    </script>
    <script type="text/javascript">
        $( document ).ready(function() {
            // Hide SubMenus.
            $(".subMenu").hide();
            
            // Shows SubMenu when it's parent is hovered.
            $(".subMenu").parent("li").hover(function () {
              $(this).find(">.subMenu").not(':animated').slideDown(300);
              $(this).toggleClass("active ");
            });
            
            // Hides SubMenu when mouse leaves the zone.
            $(".subMenu").parent("li").mouseleave(function () {
              $(this).find(">.subMenu").slideUp(150);
            });
            
            // Prevents scroll to top when clicking on <a href="#"> 
            $("a[href=\"#\"]").click(function () {
              return false;
            });
        });
    </script>
