      <div class="main-panel">
        <div class="content-wrapper">
   
          <div class="row grid-margin">
            <div class="col-lg-12">
              <div class="card px-3">
                <div class="card-body">
                  <h4 class="card-title">Media</h4>
                  <div id="lightgallery-without-thumb" class="row lightGallery">
                  <?php
                      if ($handle = opendir('./uploads/')) {
                        $i=0;
                        while (false !== ($entry = readdir($handle))) {

                            if ($entry != "." && $entry != "..") {
                              if (strpos($entry, '.png') !== false) {
                      ?>


                      <a href="#" class="image-tile">
                      <img src="<?=base_url('uploads/'.$entry)?>" alt="image small">
                    </a>



                      <?php                             
                            }
                           }
                          }

                        closedir($handle);
                      }
                      ?>




                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>