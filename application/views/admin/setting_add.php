<div class="col-md-12 col-xl-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add Setting</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-danger" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>
                                
                                <form class="cmxform" id="addSettingForm" method="post" action="<?=base_url("settings/add")?>">
                                    <fieldset>
                                    <div class="form-group">
                                        <label for="firstname">Name</label>
                                        <input id="firstname" class="form-control" name="name" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Value</label>
                                        <input id="lastname" class="form-control" name="value" type="text">
                                    </div>
                            

                                    <input class="btn btn-primary" type="submit" value="Submit">
                                    </fieldset>
                                </form>
                                </div>
                            </div>
                            </div>
                        </div>



                </div>
              </div>
            </div>