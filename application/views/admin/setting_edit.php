<div class="col-md-12 col-xl-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Data</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <?php 
                                  if($this->session->flashdata('msg')){
                                    echo "Error: ".$this->session->flashdata('msg');
                                  }
                                ?>
                                
                                <form class="cmxform" id="editSettingForm" method="post" action="<?=base_url("settings/edit")?>">
                                    <fieldset>
                                    <div class="form-group">
                                        <label for="firstname">Name</label>
                                        <input id="firstname" class="form-control" name="id" type="hidden" value="<?=$setting['id']?>">
                                        <input id="firstname" class="form-control" name="name" type="text" value="<?=$setting['name']?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Value</label>
                                        <input id="lastname" class="form-control" name="value" type="text" value="<?=$setting['value']?>">
                                    </div>
                            

                                    <input class="btn btn-primary" type="submit" value="Submit">
                                    </fieldset>
                                </form>
                                </div>
                            </div>
                            </div>
                        </div>



                </div>
              </div>
            </div>