      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Reach News</h4> 
              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_works_reach_news_add/")?>">Tambah News</a>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Content</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($pages as  $i => $page){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><?=$page['post_title']?></td>
                            <td><?=$page['content1']?></td>
                            <td>
                              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_reach_news_edit/".$page['container'])?>">Edit</a>
                            </td>
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->