<div class="main-panel">
        <div class="content-wrapper">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Become Member</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>

                                

                                <ul class="nav nav-tabs" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Bahasa</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile-1" role="tab" aria-controls="profile-1" aria-selected="false">English</a>
                                  </li>
                                </ul>


                                <div class="tab-content">
                                  <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="mediax">
                                      
                                      <div class="media-body">

                                      <form class="cmxform" id="editSettingForm" method="post" action="<?=base_url("actionPages/edit")?>"  enctype="multipart/form-data">
                                 
                                        <div class="form-group">
                                            <label for="firstname">Title</label>
                                            <input id="firstname" class="form-control" name="id-id" type="hidden" value="<?=$pageid['post_id']?>">
                                            <input id="firstname" class="form-control" name="title_id" type="text" value="<?=$pageid['post_title']?>">

                                            
								<input id="firstname" class="form-control" name="redirect-success" type="hidden" value="admin/pages_member/cb6e4c192250e0cd20bc7dfbc94b1f6e">
								<input id="firstname" class="form-control" name="redirect-error" type="hidden" value="admin/pages_member/cb6e4c192250e0cd20bc7dfbc94b1f6e">

                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Link Download</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_id1" ><?=$pageid['content1']?></textarea>
                                        </div>
                   
                                        
                                      </div> <!--media-body-->
                                    </div>
                                  </div>
                                  <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="mediax">
                                      <div class="media-body">

                                     
                                        <div class="form-group">
                                            <label for="firstname">Title</label>
                                            <input id="firstname" class="form-control" name="id-en" type="hidden" value="<?=$pageen['post_id']?>">
                                            <input id="firstname" class="form-control" name="title_en" type="text" value="<?=$pageen['post_title']?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Link Download</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_en1" ><?=$pageen['content1']?></textarea>
                                        </div>
                      


                                      </div> <!--media-body-->
                                    </div>
                                  </div>

                                    <input class="btn btn-primary" type="submit" value="Submit">
                                      
                                    </form>

                                </div>


       


                                </div>
                            </div>
                            </div>
                        </div>



                </div>
              </div>
            </div>
            </div>


            <script>
              function uploadFile1() {
                  var files = document.getElementById("file").files;
                  if(files.length > 0 ){
                      var formData = new FormData();
                      formData.append("file", files[0]);
                      var xhttp = new XMLHttpRequest();
                      // Set POST method and ajax file path
                      xhttp.open("POST", "<?=base_url('upload/own')?>", true);
                      // call on request changes state
                      xhttp.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                              var response = this.responseText;
                              if(response != 0){
                                // alert("File Upload successfully.");
                                document.getElementById("pages_home_images_id").value = response;
                              }else{
                                alert("File not uploaded.");
                              }
                          }
                      };
                      // Send request with data
                      xhttp.send(formData);
                  }else{
                      alert("Please select a file");
                  }
              }
            </script>



          <script>
              function uploadFile2() {
                  var files = document.getElementById("file2").files;
                  if(files.length > 0 ){
                      var formData = new FormData();
                      formData.append("file", files[0]);
                      var xhttp = new XMLHttpRequest();
                      // Set POST method and ajax file path
                      xhttp.open("POST", "<?=base_url('upload/own')?>", true);
                      // call on request changes state
                      xhttp.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                              var response = this.responseText;
                              if(response != 0){
                                // alert("File Upload successfully.");
                                document.getElementById("pages_home_images_en").value = response;
                              }else{
                                alert("File not uploaded.");
                              }
                          }
                      };
                      // Send request with data
                      xhttp.send(formData);
                  }else{
                      alert("Please select a file");
                  }
              }
            </script>