<div class="col-md-12 col-xl-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Anggota</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <?php 
                                  if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert- <?=$this->session->flashdata('msg-alert')?>" role="alert">
                                  <?=$this->session->flashdata('msg')?>
                                </div>
                                  <?php }?>
                                  

                                <ul class="nav nav-tabs" role="tablist">
                                  <!-- <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Bahasa</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile-1" role="tab" aria-controls="profile-1" aria-selected="false">English</a>
                                  </li> -->
                                </ul>


                                <div class="tab-content">
                                  <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="mediax">
                                      
                                      <div class="media-body">

                                      <form class="cmxform" id="editSettingForm" method="post" action="<?=base_url("actionPages/edit")?>">
                                      <input id="firstname" class="form-control" name="id-id" type="hidden" value="<?=$pageid['post_id']?>">

                                      <input id="firstname" class="form-control" name="id-en" type="hidden" value="<?=$pageen['post_id']?>">

                                        <div class="form-group">
                                            <label for="firstname">Nama</label>
                                            <input id="firstname" class="form-control" name="title_id" type="text" value="<?=$pageid['post_title']?>">
                                            <input id="firstname" class="form-control" name="type" type="hidden" value="personil">
                                            <input id="firstname" class="form-control" name="redirect-success" type="hidden" value="admin/pages_about/87dc9560c7a13615a38eed3e426a8418">
                                            <input id="firstname" class="form-control" name="redirect-error" type="hidden" value="admin/pages_personil_edit/<?=$idx?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">posisi</label>
                                            <input id="lastname" class="form-control" name="content_id1" type="text" value="<?=$pageid['content1']?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="lastname">FACEBOOK</label>
                                            <input id="lastnamxe" class="form-control" name="content_id2" value="<?=$pageid['content2']?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">INSTAGRAM</label>
                                            <input id="lastnamxe" class="form-control" name="content_id3" value="<?=$pageid['content3']?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">TWITTER</label><br>
                                            <input  class="form-control" name="content_id4" type="text" value="<?=$pageid['content4']?>">

                                        </div>
     

                                        <div class="form-group">
                                            <label for="lastname">Photo</label>
                                            <input id="images_id" class="form-control" name="content_id5" type="hidden"  value="<?=$pageid['content5']?>">
                                            <input type="file" name="file" id="file" class="btn btn-primary"  onchange="uploadFile(1)">
                                        </div>
                                        
                                      </div> <!--media-body-->
                                    </div>
                                  </div>
                                  <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="mediax">
                                      <div class="media-body">

                                     
                                        <div class="form-group">
                                            <label for="firstname">Nama</label>
                                            <input id="firstname" class="form-control" name="title_en" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Posisi</label>
                                            <input id="lastname" class="form-control" name="content_en1" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Sosmed</label>
                                            <input id="lastname" class="form-control" name="content_en2" type="text">
                                        </div>
                          
                                        <div class="form-group">
                                            <label for="lastname">Photo</label>
                                            <input id="images_en" class="form-control" name="content_en3" type="hidden" >
                                            <input type="file" name="file" id="file2" class="btn btn-primary"  onchange="uploadFile(2)">
                                        </div>

                                      </div> <!--media-body-->
                                    </div>
                                  </div>

                                  <input class="btn btn-primary" type="submit" value="Submit">
                                    
                                    </form>

                                </div>


       


                                </div>
                            </div>
                            </div>
                        </div>



                </div>
              </div>
            </div>








            <script>
              function uploadFile(uid) {
                var files;
                if(uid == 1){
                    files = document.getElementById("file").files;
                }else{
                   files = document.getElementById("file2").files;
                }
                  
                  if(files.length > 0 ){
                      var formData = new FormData();
                      formData.append("file", files[0]);
                      var xhttp = new XMLHttpRequest();
                      // Set POST method and ajax file path
                      xhttp.open("POST", "<?=base_url('upload/own')?>", true);
                      // call on request changes state
                      xhttp.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                              var response = this.responseText;
                              if(response != 0){
                                if(uid == 1){
                                  document.getElementById("images_id").value = response;
                                }else{
                                  document.getElementById("images_en").value = response;
                                }
                              }else{
                                alert("File not uploaded.");
                              }
                          }
                      };
                      // Send request with data
                      xhttp.send(formData);
                  }else{
                      alert("Please select a file");
                  }
              }
            </script>
