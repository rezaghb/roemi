<div class="col-md-12 col-xl-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit News</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">

                                <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>
                                

                                <ul class="nav nav-tabs" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Bahasa</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile-1" role="tab" aria-controls="profile-1" aria-selected="false">English</a>
                                  </li>
                                </ul>


                                <div class="tab-content">
                                  <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="mediax">
                                      
                                      <div class="media-body">

                                      <form class="cmxform" id="editSettingForm" method="post" action="<?=base_url("actionPages/edit")?>">
                                 
                                        <div class="form-group">
                                            <label for="firstname">Title</label>
                                            <input id="firstname" class="form-control" name="title_id" type="text" value="<?=$pageid['post_title']?>">
                                            <input id="firstname" class="form-control" name="type" type="hidden" value="reach-news">

                                            <input id="firstname" class="form-control" name="id-id" type="hidden" value="<?=$pageid['post_id']?>">

                                            <input id="firstname" class="form-control" name="id-en" type="hidden" value="<?=$pageen['post_id']?>">


                                            <input id="firstname" class="form-control" name="redirect-success" type="hidden" value="admin/pages_works_reach_news">
                                            <input id="firstname" class="form-control" name="redirect-error" type="hidden" value="admin/pages_reach_news_edit/<?=$idx?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Content</label>
                                            <input id="lastname" class="form-control" name="content_id1" type="text" value="<?=$pageid['content1']?>">
                                        </div>
                                        
                                      </div> <!--media-body-->
                                    </div>
                                  </div>
                                  <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="mediax">
                                      <div class="media-body">

                                     
                                        <div class="form-group">
                                            <label for="firstname">Title</label>
                                            <input id="firstname" class="form-control" name="title_en" type="text" value="<?=$pageen['post_title']?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Content</label>
                                            <input id="lastname" class="form-control" name="content_en1" type="text" value="<?=$pageen['content1']?>">
                                        </div>
                          


                                      </div> <!--media-body-->
                                    </div>
                                  </div>

                                  <input class="btn btn-primary" type="submit" value="Submit">
                                    
                                    </form>

                                </div>


       


                                </div>
                            </div>
                            </div>
                        </div>



                </div>
              </div>
            </div>