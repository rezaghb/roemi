      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Contact</h4> 
              <!-- <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_add/")?>">Add</a> -->
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Date</th>
                            <!-- <th>Actions</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($contacts as  $i => $contact){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><?=$contact['name']?></td>
                            <td><?=$contact['email']?></td>
                            <td><?=$contact['subject']?></td>
                            <td><?=$contact['message']?></td>
                            <td><?=$contact['created_at']?></td>
                            <!-- <td>
                              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_edit/".$contact['id'])?>">Edit</a>
                            </td> -->
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->