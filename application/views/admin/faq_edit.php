<div class="col-md-12 col-xl-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Data</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>

                                <ul class="nav nav-tabs" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Bahasa</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile-1" role="tab" aria-controls="profile-1" aria-selected="false">English</a>
                                  </li>
                                </ul>


                                <div class="tab-content">
                                  <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="mediax">
                                      
                                      <div class="media-body">

                                      <form class="cmxform" id="editSettingForm" method="post" action="<?=base_url("actionPages/edit")?>">
                                      <input id="firstname" class="form-control" name="id-id" type="hidden" value="<?=$pageid['post_id']?>">

                                      <input id="firstname" class="form-control" name="id-en" type="hidden" value="<?=$pageen['post_id']?>">



                                        <div class="form-group">
                                            <label for="firstname">Question</label>
                                            <input id="firstname" class="form-control" name="title_id" type="text" value="<?=$pageid['post_title']?>">
                                            <input id="firstname" class="form-control" name="type" type="hidden" value="faq">


                                            <input id="firstname" class="form-control" name="redirect-success" type="hidden" value="admin/pages_faq/349d8a8ed0ab9f2aeb9e1f76ed4111e3">
								<input id="firstname" class="form-control" name="redirect-error" type="hidden" value="admin/pages_faq_edit/<?=$idx?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Answer</label>
                                            <input id="lastname" class="form-control" name="content_id1" type="text" value="<?=$pageid['content1']?>">
                                        </div>
                                        
                                      </div> <!--media-body-->
                                    </div>
                                  </div>
                                  <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="mediax">
                                      <div class="media-body">

                                     
                                        <div class="form-group">
                                            <label for="firstname">Question</label>
                                            <input id="firstname" class="form-control" name="title_en" type="text"  value="<?=$pageen['post_title']?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Answer</label>
                                            <input id="lastname" class="form-control" name="content_en1" type="text" value="<?=$pageen['content1']?>">
                                        </div>
                          


                                      </div> <!--media-body-->
                                    </div>
                                  </div>

                                  <input class="btn btn-primary" type="submit" value="Submit">
                                    
                                    </form>

                                </div>


       


                                </div>
                            </div>
                            </div>
                        </div>



                </div>
              </div>
            </div>