      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Faq</h4> 
              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_faq_add/")?>">Add</a>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Question</th>
                            <th>Answer</th>
                            <!-- <th>Actions</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($faqs as  $i => $faq){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><?=$faq['post_title']?></td>
                            <td><?=$faq['content1']?></td>
                            <td>
                              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_faq_edit/".$faq['container'])?>">Edit</a>
                            </td>
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->