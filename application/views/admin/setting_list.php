      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Settings</h4> 
              <a class="btn btn-outline-primary" href="<?=base_url("admin/setting_add/")?>">Add</a>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>value</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($settings as  $i => $setting){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><?=$setting['name']?></td>
                            <td><?=$setting['value']?></td>
                            <td>
                              <a class="btn btn-outline-primary" href="<?=base_url("admin/setting_edit/".$setting['id'])?>">Edit</a>
                            </td>
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->