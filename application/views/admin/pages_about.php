<div class="main-panel">
        <div class="content-wrapper">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">About</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>

                                

                                <ul class="nav nav-tabs" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Bahasa</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile-1" role="tab" aria-controls="profile-1" aria-selected="false">English</a>
                                  </li>
                                </ul>


                                <div class="tab-content">
                                  <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="mediax">
                                      
                                      <div class="media-body">

                                      <form class="cmxform" id="editSettingForm" method="post" action="<?=base_url("actionPages/edit")?>">
                                 
                                        <div class="form-group">
                                            <label for="firstname">Title</label>
                                            <input id="firstname" class="form-control" name="id-id" type="hidden" value="<?=$pageid['post_id']?>">
                                            <input id="firstname" class="form-control" name="title_id" type="text" value="<?=$pageid['post_title']?>">

                                            <input id="firstname" class="form-control" name="redirect-success" type="hidden" value="admin/pages_about/87dc9560c7a13615a38eed3e426a8418">
								<input id="firstname" class="form-control" name="redirect-error" type="hidden" value="admin/pages_about/87dc9560c7a13615a38eed3e426a8418">

                                        </div>

                                        <div class="form-group">
                                            <label for="lastname">sejarah</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_id1" ><?=$pageid['content1']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">visi</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_id2" ><?=$pageid['content2']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">misi</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_id3" ><?=$pageid['content3']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">nilai</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_id4" ><?=$pageid['content4']?></textarea>
                                        </div>
                
                                        
                                      </div> <!--media-body-->
                                    </div>
                                  </div>
                                  <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="mediax">
                                      <div class="media-body">

                                     
                                        <div class="form-group">
                                            <label for="firstname">Title</label>
                                            <input id="firstname" class="form-control" name="id-en" type="hidden" value="<?=$pageen['post_id']?>">
                                            <input id="firstname" class="form-control" name="title_en" type="text" value="<?=$pageen['post_title']?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">sejarah</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_en1" ><?=$pageen['content1']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">visi</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_en2" ><?=$pageen['content2']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">misi</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_en3" ><?=$pageen['content3']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Nilai</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_en4" ><?=$pageen['content4']?></textarea>
                                        </div>

                          


                                      </div> <!--media-body-->
                                    </div>
                                  </div>

                                  <input class="btn btn-primary" type="submit" value="Submit">
                                    
                                    </form>

                                </div>


       


                                </div>
                            </div>
                            </div>
                        </div>




      <!-- partial -->
      <div class="main-panelx">
        <div class="content-wrapperx">
          <div class="cardx">
            <div class="card-body">
              <h4 class="card-title">Daftar Anggota</h4> 
              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_personil/")?>">Tambah Anggota</a>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Posisi</th>
                            <th>Sosmed</th>
                            <th>Photo</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($pages as  $i => $page){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><?=$page['post_title']?></td>
                            <td><?=$page['content1']?></td>
                            <td><?=$page['content2']?></td>
                            <td><img src="<?=base_url('uploads/'.$page['content3'])?>" width="100"></td>
                            <td>
                              <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_personil_edit/".$page['container'])?>">Edit</a>
                            </td>
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->










                </div>
              </div>
          </div>
          </div>








            