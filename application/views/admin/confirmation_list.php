      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Confirmation</h4> 
              <!-- <a class="btn btn-outline-primary" href="<?=base_url("admin/pages_add/")?>">Add</a> -->
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Bank</th>
                            <th>No Akun</th>
                            <th>Jumlah</th>
                            <th>Bukti </th>
                            <th>Tgl Trf </th>
                            <th>status </th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($pages as  $i => $page){ ?>
                        <tr>
                            <td><?=$i+1?></td>
                            <td><?=$page['post_title']?></td>
                            <td><?=$page['content1']?></td>
                            <td><?=$page['content2']?></td>
                            <td><?=$page['content3']?></td>
                            <td><?=$page['content4']?></td>
                            <td><?=$page['content5']?></td>
                            <td><?=$page['content6']?></td>
                            <td>
                            <?php if($page['content6'] == ""){?>
                              <form method="post" action="<?=base_url("actionPages/approval/")?>">
                                <input type="hidden" value="<?=$page['post_id']?>" name="id">
                                <input type="submit" value="Approval" class="btn btn-outline-primary">
                              </form>
                              <?php }?>
                            </td>
                        </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->