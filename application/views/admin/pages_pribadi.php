<div class="main-panel">
        <div class="content-wrapper">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Donor Pribadi</h4>

                  <div class="row">
                            <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>

                                

                                <ul class="nav nav-tabs" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Bank</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile-1" role="tab" aria-controls="profile-1" aria-selected="false">QR Code</a>
                                  </li>
                                </ul>


                                <div class="tab-content">
                                  <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="mediax">
                                      
                                      <div class="media-body">

                                      <form class="cmxform" id="editSettingForm" method="post" action="<?=base_url("actionPages/edit")?>"  enctype="multipart/form-data">
                                 
                                        <div class="form-group">
                                            <label for="firstname">Title</label>
                                            <input id="firstname" class="form-control" name="id-id" type="hidden" value="<?=$pageid['post_id']?>">
                                            <input id="firstname" class="form-control" name="title_id" type="text" value="<?=$pageid['post_title']?>">

                                            <input id="firstname" class="form-control" name="redirect-success" type="hidden" value="admin/pages_pribadi/0ce90809ee7460fcce92fcbf3ac263d2">
								<input id="firstname" class="form-control" name="redirect-error" type="hidden" value="admin/pages_pribadi/0ce90809ee7460fcce92fcbf3ac263d2">

                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">No Rekening</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_id1" ><?=$pageid['content1']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Atas Nama</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_id2" ><?=$pageid['content2']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Bank</label>
                                            <textarea id="lastnamxe" class="form-control" name="content_id3" ><?=$pageid['content3']?></textarea>
                                        </div>


                                        
                                      </div> <!--media-body-->
                                    </div>
                                  </div>
                                  <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="mediax">
                                      <div class="media-body">

                                     
                                        <div class="form-group">
                                            <label for="firstname">Title</label>
                                            <input id="firstname" class="form-control" name="id-en" type="hidden" value="<?=$pageen['post_id']?>">
                                            <input id="firstname" class="form-control" name="title_en" type="text" value="<?=$pageen['post_title']?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="lastname">Image</label><br>
                                            <input id="images_en" class="form-control" name="content_en1" type="hidden" value="<?=$pageen['content1']?>">
                                            <img src="<?=base_url("uploads/".$pageen['content1'])?>" width="150">
                                            <input type="file" name="file" id="file2" class="btn btn-primary"  onchange="uploadFile(2)">
                                        </div>


                                      </div> <!--media-body-->
                                    </div>
                                  </div>

                                    <input class="btn btn-primary" type="submit" value="Submit">
                                      
                                    </form>

                                </div>


       


                                </div>
                            </div>
                            </div>
                        </div>



                </div>
              </div>
            </div>
            </div>




            <script>
              function uploadFile(uid) {
                var files;
                if(uid == 1){
                    files = document.getElementById("file").files;
                }else{
                   files = document.getElementById("file2").files;
                }
                  
                  if(files.length > 0 ){
                      var formData = new FormData();
                      formData.append("file", files[0]);
                      var xhttp = new XMLHttpRequest();
                      // Set POST method and ajax file path
                      xhttp.open("POST", "<?=base_url('upload/own')?>", true);
                      // call on request changes state
                      xhttp.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                              var response = this.responseText;
                              if(response != 0){
                                if(uid == 1){
                                  document.getElementById("images_id").value = response;
                                }else{
                                  document.getElementById("images_en").value = response;
                                }
                              }else{
                                alert("File not uploaded.");
                              }
                          }
                      };
                      // Send request with data
                      xhttp.send(formData);
                  }else{
                      alert("Please select a file");
                  }
              }
            </script>
