<?php 
       $logo = $this->m_settings->getByName("logo");
       $address = $this->m_settings->getByName("address");
       $telp = $this->m_settings->getByName("telp");
       $email = $this->m_settings->getByName("email");
       $fb = $this->m_settings->getByName("fb");
       $tw = $this->m_settings->getByName("twitter");
       $ig = $this->m_settings->getByName("ig");
       ?>


<!-- Bio -->
<div class="main-content margin-custome">
<div class="sec-title3 text-center mb-50">
        <h2 class="title">APA Itu Roemi ?</h2>
    </div>
    <!-- video -->
    <div id="rs-about-video" class="rs-about-video pt-10 pb-10 md-pt-10 md-pb-10">
        <div class="container">
            <div class="video-img-part media-icon orange-color2">
                <img src="<?= base_url()?>assets/images/about/about-video-bg-orange.png" alt="Video Bg Image">
                <a class="popup-videos" href="https://www.youtube.com/watch?v=atMUy_bPoQI">
                    <i class="fa fa-play"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- end video -->


    <div class="rs-slider main-home">
        <div class="container">
            <div class="sec-title mb-50 md-mb-30 text-center">
                <i class="fa fa-angle-down fa-3x" aria-hidden="true"></i>
            </div>
        </div>
    </div>

    <!--  banner -->
    <div class="rs-slider main-home">
        <div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="0" data-autoplay="true"
            data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false"
            data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1"
            data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="1"
            data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1"
            data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="1" data-md-device-nav="true"
            data-md-device-dots="false">
            <div class="slider-content slide1">
                <div class="container">
                    <div class="content-part">
                        <div class="sl-sub-title wow bounceInLeft" data-wow-delay="300ms" data-wow-duration="2000ms">
                            Start to learning today</div>
                        <h1 class="sl-title wow fadeInRight" data-wow-delay="600ms" data-wow-duration="2000ms">Online
                            Courses From Leading Experts</h1>
                        <div class="sl-btn wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2000ms">
                            <!-- <a class="readon orange-btn main-home" href="#">Find Courses</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-content slide2">
                <div class="container">
                    <div class="content-part">
                        <div class="sl-sub-title wow bounceInLeft" data-wow-delay="300ms" data-wow-duration="2000ms">
                            Start to learning today</div>
                        <h1 class="sl-title wow fadeInRight" data-wow-delay="600ms" data-wow-duration="2000ms">Explore
                            Interests and Career With Courses</h1>
                        <div class="sl-btn wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2000ms">
                            <!-- <a class="readon orange-btn main-home" href="#">Find Courses</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  end banner-->
    <!-- visi -->
    <!-- <div class="rs-slider main-home">
        <div class="container">
            <div class="sec-title mb-50 md-mb-30 text-center">
                <h2 class="title mb-0">Visi</h2>
                <p><?=$page['content2']?></p>
            </div>
        </div>
    </div> -->
    <!-- end visi -->
    <!-- misi -->
    <!-- <div class="rs-slider main-home">
        <div class="container">
            <div class="sec-title mb-50 md-mb-30 text-center">
                <h2 class="title mb-0">Misi</h2>
                <p><?=$page['content3']?></p>
            </div>
        </div>
    </div> -->
    <!-- misi end -->
    <!-- Team Section Start -->
    <div id="rs-team" class="rs-team style1 inner-style orange-color pt-94 pb-100 md-pt-64 md-pb-70 gray-bg">
        <div class="container">
            <div class="sec-title mb-50 md-mb-30 text-center">
                <h2 class="title mb-0">Members</h2>
            </div>
            <div class="row">
                <?php foreach($personils as  $i => $personil){ ?>
                <div class="col-lg-4 col-sm-6 mb-30">
                    <div class="team-item">
                        <img src="<?= base_url("uploads/".$personil['content5'])?>" alt="">
                        <div class="content-part">
                            <h4 class="name"><a href="team-single.html"><?=$personil['post_title']?></a></h4>
                            <span class="designation"><?=$personil['content1']?></span>
                            <ul class="social-links">
                                <!-- <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
    <!-- Team Section End -->

    <!-- maps -->
    <div class="contact-page-section pt-100 pb-100 md-pt-70 md-pb-70">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 md-mb-30">
                    <!-- Map Section Start -->
                    <div class="contact-map3">
                        <iframe
                            src="https://maps.google.com/maps?q=Fort%20Miley&t=&z=13&ie=UTF8&iwloc=&output=embed"></iframe>
                    </div>
                </div>
                <div class="col-lg-6 pl-60 md-pl-15">
                    <div class="contact-address-section style2">
                        <div class="contact-info mb-15 md-mb-30">
                            <div class="icon-part">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="content-part">
                                <h5 class="info-subtitle">Address</h5>
                                <h4 class="info-title"><?=$address['value']?></h4>
                            </div>
                        </div>
                        <div class="contact-info mb-15 md-mb-30">
                            <div class="icon-part">
                                <i class="fa fa-envelope-open-o"></i>
                            </div>
                            <div class="content-part">
                                <h5 class="info-subtitle">Email Address</h5>
                                <h4 class="info-title"><a href="hello@roemi.id><?=$email['value']?></a></h4>
                            </div>
                        </div>
                        <div class="contact-info">
                            <div class="icon-part">
                                <i class="fa fa-headphones"></i>
                            </div>
                            <div class="content-part">
                                <h5 class="info-subtitle">Phone Number</h5>
                                <h4 class="info-title"><a href="tel+021276564385"><?=$telp['value']?></a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end maps -->
    <!-- FAq -->
    <div class="contact-page-section pt-70 pb-70 md-pt-170 md-pb-10">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 md-mb-30">
                    <div class="main-part">
                        <div class="title mb-10 md-mb-15">
                            <h2 class="text-part">Frequently Asked Questions</h2>
                        </div>
                        <div class="faq-content">
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item card">
                                    <?php foreach($faqs as  $i => $faq){ ?>
                                    <div class="accordion-header card-header" id="headingOne">
                                        <button class="accordion-button card-link" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne"><?=$faq['post_title']?></button>
                                    </div>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                        aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body card-body"> <?=$faq['content1']?></div>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pt-10 pb-10 md-pt-170 md-pb-10"></div>
                    <div class="pt-10 pb-10 md-pt-90 md-pb-10"></div>
                </div>
                <div class="col-lg-6 pl-60 md-pl-15">
                    <div class="contact-comment-box">
                        <!-- <div class="inner-part">
                            <h2 class="title mb-mb-15">Get In Touch</h2>
                            <p>Have some suggestions or just want to say hi? Our support team are ready to help you
                                24/7.</p>
                        </div> -->
                        <div id="form-messages"></div>
                        <form id="contact-form" method="post" action="mailer.php">
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-6 mb-35 col-md-6 col-sm-6">
                                        <input class="from-control" type="text" id="name" name="name" placeholder="Name"
                                            required="">
                                    </div>
                                    <div class="col-lg-6 mb-35 col-md-6 col-sm-6">
                                        <input class="from-control" type="text" id="email" name="email"
                                            placeholder="Email" required="">
                                    </div>
                                    <div class="col-lg-12 mb-35 col-md-12 col-sm-12">
                                        <input class="from-control" type="text" id="subject" name="subject"
                                            placeholder="Subject" required="">
                                    </div>
                                    <div class="col-lg-12 mb-50">
                                        <textarea class="from-control" id="message" name="message"
                                            placeholder=" Message" required=""></textarea>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <input class="btn-send" type="submit" value="Send">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- end bio -->