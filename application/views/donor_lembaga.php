		<!-- Main content Start -->
		<div class="main-content">
			<!-- Breadcrumbs Start -->
			<div class="rs-breadcrumbs breadcrumbs-overlay">
					<div class="breadcrumbs-img">
							<img src="<?=base_url()?>assets/images/breadcrumbs/2.jpg" alt="Breadcrumbs Image">
					</div>
					<div class="breadcrumbs-text white-color">
							<h1 class="page-title">Donasi Lembaga</h1>
							<!-- <ul>
								<li>
									<a class="active" href="index.html">Home</a>
								</li>
								<li>About Us</li>
							</ul> -->
					</div>
			</div>
			<!-- Breadcrumbs End -->

			<!-- About Section Start -->
			<div id="rs-about" class="rs-about style1 pt-100 pb-100 md-pt-70 md-pb-70" style="background: #fff">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6 order-last padding-0 md-pl-15 md-pr-15 md-mb-30">
							<div class="img-part">
								<img class="" src="<?=base_url()?>assets/images/about/about2orange.png" alt="About Image">
							</div>
						</div>
						<div class="col-lg-6 pr-70 md-pr-15">
							<div class="sec-title">
								<h2 class="title mb-17"><?=$page['post_title']?></h2>
								<div class="bold-text mb-22">Contact Number <?=$page['content1']?></div>
								<!-- <div class="desc">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed eius to mod tempors incididunt ut labore et dolore magna this aliqua  enims ad minim. Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed eius to mod tempor. Lorem ipsum dolor sit amet</div>
                             -->
                             <a href="<?=base_url('uploads/'.$page['content2'])?>">Dokument Tata cara donasi untuk lembaga</a>


                             <div class="banner-btn wow fadeInUp" data-wow-delay="1500ms" data-wow-duration="2000ms">
                                        <a class="readon banner-style" href='<?=base_url("index.php/pages/konfirmasi")?>'>Konfirmasi</a>
                                    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- About Section End -->
    </div>