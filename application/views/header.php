<?php
$lang = $this->session->userdata('language');
$logo = $this->m_settings->getByName("logo");
$address = $this->m_settings->getByName("address");
$telp = $this->m_settings->getByName("telp");
$email = $this->m_settings->getByName("email");

$home ="Beranda";
$about ="Bio";
$works ="Inisiatif";
$resources ="Galeri";
$partners ="Kolaborator";
$involved ="Yuk Ikut";
$getInTouch ="Hubungi Kami";

$active_home ="";
$active_about ="";
$active_works ="";
$active_resources ="";
$active_partners ="";
$active_involved ="";
$active_intouch ="";

if($lang == "en"){
    $home ="Home";
    $about ="About Us";
    $works ="Our Works";
    $resources ="Resources";
    $partners ="Partners";
    $involved ="Involved";
    $getInTouch ="Get in Touch";

}
$segment = $this->uri->segment(2);
if($segment == "") {
    $active_home = "current-menu-item";
}
if($segment == "about") {
    $active_about = "current-menu-item";
}
if($segment == "works") {
    $active_works = "current-menu-item";
}
if($segment == "resources") {
    $active_resources = "current-menu-item";
}
if($segment == "partners") {
    $active_partners = "current-menu-item";
}
if($segment == "involved") {
    $active_involved = "current-menu-item";
}
if($segment == "contact") {
    $active_intouch = "current-menu-item";
}


?>

<!DOCTYPE html>
<html lang="english">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>ROEMI - Official Website</title>
    <meta name="description" content="">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url()?>assets/images/load.png">
    <!-- Bootstrap v5.0.2 css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/bootstrap.min.css">
    <!-- font-awesome css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/animate.css">
    <!-- owl.carousel css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/owl.carousel.css">
    <!-- slick css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/slick.css">
    <!-- off canvas css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/off-canvas.css">
    <!-- linea-font css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/fonts/linea-fonts.css">
    <!-- flaticon css  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/fonts/flaticon.css">
    <!-- magnific popup css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/magnific-popup.css">
    <!-- Main Menu css -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/rsmenu-main.css">
    <!-- spacing css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/rs-spacing.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>style.css">
    <!-- This stylesheet dynamically changed from style.less -->
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/responsive.css">
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="home-style3">

    <!--Preloader area start here-->
    <div id="loader" class="loader">
        <div class="loader-container">
            <div class='loader-icon'>
                <img src="<?= base_url()?>assets/images/load.png" alt="">
            </div>
        </div>
    </div>
    <!--Preloader area End here-->

    <!--Full width header Start-->
    <div class="full-width-header header-style2 modify1">
        <!--Header Start-->
        <header id="rs-header" class="rs-header">
            <!-- Topbar Area Start -->
            <div class="topbar-area d-none">
                <div class="container">
                    <div class="row y-middle">
                        <div class="col-md-6">
                            <ul class="topbar-contact">
                                <li>
                                    <i class="flaticon-email"></i>
                                    <a href="mailto:">s</a>
                                </li>
                                <li>
                                    <i class="flaticon-call"></i>
                                    <a href="tel:+0885898745"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 text-end">
                            <ul class="topbar-right">
                                <li class="login-register">
                                    <i class="fa fa-sign-in"></i>
                                    <a href="login.html">Login</a>/<a href="register.html">Register</a>
                                </li>
                                <li class="btn-part">
                                    <a class="apply-btn" href="#">Apply Now</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Topbar Area End -->

            <!-- Menu Start -->
            <div class="menu-area menu-sticky">
                <div class="container-fluid">
                    <div class="row y-middle">
                        <div class="col-lg-2">
                            <div class="logo-cat-wrap">
                                <div class="logo-part pr-90">
                                    <a href="<?= base_url()?>">
                                        <img src="<?= base_url()?>assets/images/roemilogo.png" alt="">
                                    </a>
                                </div>

                                <!-- <div class="categories-btn">
                                       <button type="button" class="cat-btn"><i class="fa fa-th"></i>Categories</button>
                                        <div class="cat-menu-inner">
                                            <ul id="cat-menu">
                                                <li><a href="">Category 1</a></li>
                                                <li><a href="">Category 2</a></li>
                                                <li><a href="">Category 3</a></li>
                                                <li><a href="">Category 4</a></li>
                                            </ul>
                                        </div>
                                    </div> -->
                            </div>
                        </div>
                        <div class="col-lg-10 text-center">
                            <div class="rs-menu-area">
                                <div class="main-menu pr-90 md-pr-15">
                                    <div class="mobile-menu" style="display: flex; justify-content: space-between;">

                                        <a class="rs-menu-toggle" style="padding-left: 80%">
                                            <!-- <i class="fa fa-flag"></i> -->
                                        </a>

                                        <a class="rs-menu-toggle">
                                            <i class="fa fa-bars"></i>
                                        </a>
                                    </div>
                                    <nav class="rs-menu">
                                        <ul class="nav-menu">
                                            <li class="mega-rs <?= $active_about ?>"> <a
                                                    href="<?= base_url("pages/about/".$lang)?>"><?=$about?></a>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="<?= base_url('pages/inisiatif/'.$lang)?>">Inisiatif</a>
                                                <ul class="sub-menu">
                                                    <li><a href="<?= base_url('pages/reach/'.$lang)?>">REACH</a> </li>
                                                    <li><a href="<?= base_url('pages/koper/'.$lang)?>">INI KOPER</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="<?= base_url('pages/foto/'.$lang)?>">Galeri</a>
                                                <ul class="sub-menu">
                                                    <li><a href="<?= base_url('pages/foto/'.$lang)?>">Foto</a> </li>
                                                    <li><a href="<?= base_url('pages/video/'.$lang)?>">Video</a> </li>
                                                    <li><a href="<?= base_url('pages/audio/'.$lang)?>">Audio</a> </li>
                                                    <li><a href="<?= base_url('pages/meterial/'.$lang)?>">Material</a>
                                                    </li>
                                                    <li><a href="<?= base_url('pages/news/'.$lang)?>">News</a> </li>
                                                    <li><a href="<?= base_url('pages/event/'.$lang)?>">Event</a> </li>
                                                </ul>
                                            </li>
                                            <li class="mega-rs <?= $active_partners ?>"><a
                                                    href="<?= base_url('pages/kolaborator/'.$lang)?>"><?=$partners?></a>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="<?= base_url('pages/join/'.$lang)?>">Yuk Ikut</a>
                                                <ul class="sub-menu">
                                                    <li><a href="<?= base_url('pages/join/'.$lang)?>">Jadi Anggota</a>
                                                    </li>
                                                    <li><a href="<?= base_url('pages/fans/'.$lang)?>">Jadi
                                                            Pendukung</a>
                                                    </li>
                                                    <li><a href="<?= base_url('pages/investor/'.$lang)?>">Jadi
                                                            Investor</a></li>
                                                </ul>
                                            </li>



                                            <li class="menu-item-has-children">
                                                <a class="cart-icon">
                                                    <?php
                                                            if($lang == "id"){?>Ind
                                                    <img src="<?=base_url()?>assets/images/indonesia.png" width="16">
                                                    <?php }else{?>Eng
                                                    <img src="<?=base_url()?>assets/images/uk.png" width="16">
                                                    <?php }?>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li><a href="<?=base_url("language/change/id")?>">Indonesia
                                                            <img src="<?=base_url()?>assets/images/indonesia.png"
                                                                width="16"></a> </li>
                                                    <li><a href="<?=base_url("language/change/en")?>">English
                                                            <img src="<?=base_url()?>assets/images/uk.png" width="16">
                                                        </a></li>
                                                </ul>
                                                <!-- </li> -->
                                            </li>


                                        </ul> <!-- //.nav-menu -->
                                    </nav>
                                </div> <!-- //.main-menu -->
                                <!-- <div class="expand-btn-inner">
                                    <ul>
                                        <li class="icon-bar cart-inner no-border mini-cart-active">
                                            <a class="cart-icon">
                                                <?php
                                                    if($lang == "id"){?>
                                                <img src="<?=base_url()?>assets/images/indonesia.png" width="16" 
                                                   >
                                                <?php }else{?>
                                                <img src="<?=base_url()?>assets/images/uk.png" width="16"
                                                   >
                                                <?php }?>
                                            </a>
                                            <div class="woocommerce-mini-cart text-start">
                                                <div class="cart-bottom-part">
                                                    <div class="cart-btn text-center">
                                                        <a class="crt-btn btn1"
                                                            href="<?=base_url("language/change/id")?>">
                                                            <img src="<?=base_url()?>assets/images/indonesia.png"
                                                                width="16">
                                                        </a>
                                                        <a class="crt-btn btn2"
                                                            href="<?=base_url("language/change/en")?>">
                                                            <img src="<?=base_url()?>assets/images/uk.png" width="16">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Menu End -->

            <!-- Canvas Menu start -->
            <nav class="right_menu_togle hidden-md">
                <div class="close-btn">
                    <div id="nav-close">
                        <div class="line">
                            <span class="line1"></span><span class="line2"></span>
                        </div>
                    </div>
                </div>
                <div class="canvas-logo">
                    <a href="index.html"><img src="<?= base_url()?>assets/images/roemi-dark.png" alt="logo"></a>
                </div>
                <div class="offcanvas-text">
                    <p>We denounce with righteous indige nationality and dislike men who are so beguiled and demo by the
                        charms of pleasure of the moment data com so blinded by desire.</p>
                </div>
                <div class="offcanvas-gallery">
                    <div class="gallery-img">
                        <a class="image-popup" href="<?= base_url()?>assets/images/gallery/1.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/1.jpg" alt=""></a>
                    </div>
                    <div class="gallery-img">
                        <a class="image-popup" href="<?= base_url()?>assets/images/gallery/2.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/2.jpg" alt=""></a>
                    </div>
                    <div class="gallery-img">
                        <a class="image-popup" href="<?= base_url()?>assets/images/gallery/3.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/3.jpg" alt=""></a>
                    </div>
                    <div class="gallery-img">
                        <a class="image-popup" href="<?= base_url()?>assets/images/gallery/4.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/4.jpg" alt=""></a>
                    </div>
                    <div class="gallery-img">
                        <a class="image-popup" href="<?= base_url()?>assets/images/gallery/5.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/5.jpg" alt=""></a>
                    </div>
                    <div class="gallery-img">
                        <a class="image-popup" href="<?= base_url()?>assets/images/gallery/6.jpg"><img
                                src="<?= base_url()?>assets/images/gallery/6.jpg" alt=""></a>
                    </div>
                </div>
                <div class="map-img">
                    <img src="<?= base_url()?>assets/images/roemi-map.jpg" alt="">
                </div>
                <div class="canvas-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </nav>
            <!-- Canvas Menu end -->
        </header>
        <!--Header End-->
    </div>
    <!--Full width header End-->