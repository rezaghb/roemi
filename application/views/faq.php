
		<!-- Main content Start -->
        <div class="main-content">


            <!-- Counter Section Start -->
            <div class="main-content">
            <!-- Breadcrumbs Start -->
            <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="<?= base_url()?>assets/images/banner/home1.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title">FAQ</h1>
                    <!-- <ul>
                        <li>
                            <a class="active" href="index.html">Home</a>
                        </li>
                        <li>Faq</li>
                    </ul> -->
                </div>
            </div>
            <!-- Breadcrumbs End -->

            <div class="rs-faq-part orange-color pt-100 pb-100 md-pt-70 md-pb-70">
                 <div class="container">
                     <div class="content-part mb-50 md-mb-30">
                         <div class="title mb-40 md-mb-15">
                             <h3 class="text-part">FAQ</h3>
                         </div>
                         <div id="accordion" class="accordion">
                         
                         <?php foreach($faqs as  $i => $faq){ ?>
                         
                            <div class="card">
                                <div class="card-header">
                                    <a class="card-link" href="#" data-bs-toggle="collapse" data-bs-target="#collapseOne" area-expanded="true"><?=$faq['post_title']?></a>
                                </div>
                                <div id="collapseOne" class="collapse show" data-bs-parent="#accordion">
                                    <div class="card-body">
                                    <?=$faq['content1']?>
                                    </div>
                                </div>
                            </div>
                            <?php }?>

                         </div>
                     </div>
     
                 </div>
            </div>
        </div> 
            <!-- Counter Section End -->

        </div> 
        <!-- Main content End --> 