<!-- Bio -->
<div class="main-content margin-custome">
    <div class="sec-title3 text-center mb-50">
        <h2 class="title">REACH</h2>
    </div>

    <!-- video -->
    <div id="rs-about-video" class="rs-about-video pt-10 pb-10 md-pt-10 md-pb-10">
        <div class="container">
            <div class="video-img-part media-icon orange-color2">
                <img src="<?= base_url()?>assets/images/roemi/4.png" alt="Video Bg Image">
                <a class="popup-videos" href="https://www.youtube.com/watch?v=atMUy_bPoQI">
                    <i class="fa fa-play"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- end video -->
    <!--  banner -->
    <div class="rs-slider main-home">
        <div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="0" data-autoplay="true"
            data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false"
            data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1"
            data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="1"
            data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1"
            data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="1" data-md-device-nav="true"
            data-md-device-dots="false">
            <div class="slider-content slide1">
                <div class="container">
                    <div class="content-part">
                        <div class="sl-sub-title wow bounceInLeft" data-wow-delay="300ms" data-wow-duration="2000ms">
                            Start to learning today</div>
                        <h1 class="sl-title wow fadeInRight" data-wow-delay="600ms" data-wow-duration="2000ms">Online
                            Courses From Leading Experts</h1>
                        <div class="sl-btn wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2000ms">
                            <!-- <a class="readon orange-btn main-home" href="#">Find Courses</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-content slide2">
                <div class="container">
                    <div class="content-part">
                        <div class="sl-sub-title wow bounceInLeft" data-wow-delay="300ms" data-wow-duration="2000ms">
                            Start to learning today</div>
                        <h1 class="sl-title wow fadeInRight" data-wow-delay="600ms" data-wow-duration="2000ms">Explore
                            Interests and Career With Courses</h1>
                        <div class="sl-btn wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2000ms">
                            <!-- <a class="readon orange-btn main-home" href="#">Find Courses</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  end banner-->
    <div id="rs-blog" class="rs-blog main-home pb-20 pt-20 md-pt-20 md-pb-20">
        <div class="container">
            <div class="sec-title3 text-center mb-50">
                <h2 class="title"> <q>Ekpresikan dirimu. Cari Partner Kolaborasimu di REACH </q></h2>
            </div>
            <h2 class="readon blue-btn main-home btnMore"><a href="<?= base_url('pages/reach/'.$this->lang)?>"
                    style="color: #fff">Download iOS</a></h2>
            <h2 class="readon blue-btn main-home btnMore"><a href="<?= base_url('pages/reach/'.$this->lang)?>"
                    style="color: #fff">Download Android</a></h2>
        </div>
    </div>


</div>
<!-- end bio -->