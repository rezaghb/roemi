<?php 
       $logo = $this->m_settings->getByName("logo");
       $address = $this->m_settings->getByName("address");
       $telp = $this->m_settings->getByName("telp");
       $email = $this->m_settings->getByName("email");
       $fb = $this->m_settings->getByName("fb");
       $tw = $this->m_settings->getByName("twitter");
       $ig = $this->m_settings->getByName("ig");
       ?>
       


<!-- Main content Start -->
    <div class="main-content">
    <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="<?= base_url()?>assets/images/banner/home1.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title">Get In Touch</h1>
                    <ul>
                        <p>
                            <!-- <img src="<?= base_url()?>assets/images/logo.png" style="width: 200px"> -->
                    </p>
                    </ul>
                </div>
            </div>
         

    		<!-- Contact Section Start -->
    		<div class="contact-page-section orange-color pt-100 pb-100 md-pt-70 md-pb-70" style="background: #fff">
            	<div class="container">


                    <div class="row rs-contact-box mb-90 md-mb-50">
        				<div class="col-lg-4 col-md-12-4 lg-pl-0 sm-mb-30 md-mb-30">
        					<div class="address-item">
        						<div class="icon-part">
        							<img src="<?= base_url()?>assets/images/facebook-logo.png" alt="">
        						</div>
        						<div class="address-text">
                                    <a class="label" href="<?=$sosmed['post_title']?>">Facebook</a>
                                    <!-- <span class="des">228-5 Main Street, Georgia, USA</span> -->
                                </div>
        					</div>
        				</div>
                        <div class="col-lg-4 col-md-12 lg-pl-0 sm-mb-30 md-mb-30">
                            <div class="address-item">
                                <div class="icon-part">
                                    <img src="<?= base_url()?>assets/images/logo-ig.png" alt="">
                                </div>
                                <div class="address-text">
                                    <a class="label"  href="<?=$sosmed['content2']?>">Instagram</a>
                                    <!-- <span class="des"><a href="mailto:info@rstheme.com">info@rstheme.com</a></span> -->
                                </div>
                            </div>
                        </div> 
                        <div class="col-lg-4 col-md-12 lg-pl-0 sm-mb-30">
                            <div class="address-item">
                                <div class="icon-part">
                                    <img src="<?= base_url()?>assets/images/logo-twitter.png" alt="">
                                </div>
                                <div class="address-text">
                                    <a class="label"  href="<?=$sosmed['content3']?>">Twitter</a>
                                    <!-- <span class="des"><a href="tel+0885898745">(+088)589-8745</a></span> -->
                                </div>
                            </div>
                        </div>
            		</div>





            		<div class="row align-items-center pb-50 rs-event home12style">
                        <div class="col-lg-4 md-mb-50">
                            <div class="contact-address-section style2">  
                
            					<div class="contact-info mb-15 md-mb-30">
            						<div class="icon-part">
            							<i class="fa fa-home"></i>
            						</div>
            						<div class="content-part">
    	        						<h5 class="info-subtitle">Address</h5>
    	        						<h4 class="info-title"><?=$address['value']?></h4>
    	        					</div>
            					</div>
            					<div class="contact-info mb-15 md-mb-30">
            						<div class="icon-part">
            							<i class="fa fa-envelope-open-o"></i>
            						</div>
            						<div class="content-part">
    	        						<h5 class="info-subtitle">Email Address</h5>
    	        						<h4 class="info-title"><a href="mailto:info@rstheme.com"><?=$email['value']?></a></h4>
    	        					</div>
            					</div>
            					<div class="contact-info">
            						<div class="icon-part">
            							<i class="fa fa-headphones"></i>
            						</div>
            						<div class="content-part">
    	        						<h5 class="info-subtitle">Phone Number</h5>
    	        						<h4 class="info-title"><a href="tel+021-27656438"><?=$telp['value']?></a></h4>
    	        					</div>
            					</div>
                			</div>
                        </div>
                        <div class="col-lg-8">
                            <!-- Map Section Start --> 
                            <div class="contact-map">
                            <div class="mapouter"><div class="gmap_canvas"><iframe width="741" height="600" id="gmap_canvas" src="https://maps.google.com/maps?q=Grha%20Tirtadi%20Jalan%20Pangeran%20Antasari%20No.%2018A%20Unit%20405%20Jakarta%20Selatan%20-%2012150&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net/blog/divi-discount-code-elegant-themes-coupon/"></a><br><style>.mapouter{position:relative;text-align:right;height:620px;width:741px;}</style><a href="https://www.embedgooglemap.net"></a><style>.gmap_canvas {overflow:hidden;background:none!important;height:600px;width:741px;}</style></div></div>
                            </div>
                            <!-- Map Section End -->
                        </div>
            		</div>

            		<div class="row align-items-end contact-bg1">
            			<div class="col-lg-4 md-pt-50 lg-pr-0">
            				<div class="contact-image">
            					<img src="<?= base_url()?>assets/images/contact/2.png" alt="Contact Images">
            				</div>
            			</div>
            			<div class="col-lg-8 lg-pl-0">
			        		<div class="rs-quick-contact new-style">
                                <div class="inner-part mb-35">
                                    <h2 class="title mb-15">Get In Touch <?=$this->session->flashdata('msg-alert')?></h2>
                                    <p>    

                                    <?php 
                                if($this->session->flashdata('msg')){
                                ?>
                                <div class="alert alert-<?=$this->session->flashdata('msg-alert')?>" role="alert">
                                <?=$this->session->flashdata('msg')?>
                                </div>
                                <?php }?>


                                </p>
                                </div>
                                <div id="form-messages"></div>
                                <form id="contact-formx" method="post" action="<?= base_url('contact/add')?>">
                                    <div class="row">
                                        <div class="col-lg-6 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="text" id="name" name="name" placeholder="Name" required="">
                                        </div> 
                                        <div class="col-lg-6 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="email" id="email" name="email" placeholder="Email" required="">
                                        </div>   

                                        <div class="col-lg-12 mb-30 col-md-6 col-sm-6">
                                            <input class="from-control" type="text" id="subject" name="subject" placeholder="Subject" required="">
                                        </div>
                                     
                                        <div class="col-lg-12 mb-40">
                                            <textarea class="from-control" id="message" name="message" placeholder=" Message" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <input class="btn-send" type="submit" value="Submit Now">
                                    </div>       
                                </form>
                            </div> 
            			</div>
            		</div>
            	</div>
            </div>
            <!-- Contact Section End -->  

            <!-- Newsletter section start -->
            <div class="rs-newsletter style1 orange-color mb--90 sm-mb-0 sm-pb-70">
                <div class="container">
                    <div class="newsletter-wrap">
                        <div class="row y-middle">
                            <div class="col-lg-6 col-md-12 md-mb-30">
                               <div class="content-part">
                                   <div class="sec-title">
                                       <div class="title-icon md-mb-15">
                                           <img src="<?= base_url()?>assets/images/newsletter.png" alt="images">
                                       </div>
                                       <h2 class="title mb-0 white-color"><a href="<?= base_url('pages/faq')?>" alt="images">Frequenly Ask Question</a></h2>
                                   </div>
                               </div>
                            </div>
              
                        </div>
                    </div>
                </div>
            </div>
            <!-- Newsletter section end -->
        </div> 

        <br>
        <br>
        <br>
        <!-- Main content End --> 