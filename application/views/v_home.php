<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

<!-- Main content Start -->
<div class="main-content">
    <!-- banner -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="<?= base_url()?>assets/images/roemi/6.PNG" alt="Breadcrumbs Image">
                </div>
                <!-- <div class="breadcrumbs-text white-color padding">
                    <h1 class="page-title">Connection.Collaboration.Change</h1>
                    <a href="#slider-homepage"><i class="fa fa-angle-down fa-3x" aria-hidden="true"></i></a>
                </div> -->
            </div>

</div>





<div class="rs-slider main-home" id="slider-homepage">

    <div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="0" data-autoplay="true"
        data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false"
        data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false"
        data-mobile-device-dots="false" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="false"
        data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="1"
        data-md-device-nav="true" data-md-device-dots="false">
        <div class="slider-content slide1">
            <div class="container">
                <div class="content-part">
                    <div class="sl-sub-title wow bounceInLeft" data-wow-delay="300ms" data-wow-duration="2000ms">
                        Start to learning today</div>
                    <h1 class="sl-title wow fadeInRight" data-wow-delay="600ms" data-wow-duration="2000ms">Online
                        Courses From Leading Experts</h1>
                    <div class="sl-btn wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2000ms">
                        <a class="readon orange-btn main-home" href="#">Find Courses</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-content slide2">
            <div class="container">
                <div class="content-part">
                    <div class="sl-sub-title wow bounceInLeft" data-wow-delay="300ms" data-wow-duration="2000ms">
                        Start to learning today</div>
                    <h1 class="sl-title wow fadeInRight" data-wow-delay="600ms" data-wow-duration="2000ms">Explore
                        Interests and Career With Courses</h1>
                    <div class="sl-btn wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2000ms">
                        <a class="readon orange-btn main-home" href="#">Find Courses</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Features Section start -->
    <div id="rs-features" class="rs-features main-home">
        <div class="container">
            <div class="row">
            </div>
        </div>
    </div>
    <!-- Features Section End -->
</div>
<!-- end banner -->
<!-- reach and koper kontent -->
<div id="rs-categories" class="rs-categories main-home pt-30 pb-30 md-pt-30 md-pb-30">
    <div class="container">
        <div class="sec-title3 text-center mb-45">
            <h2 class="title black-color">INISIATIF</h2>
        </div>
        <div class="row mb-35">
            <div class="col-lg-6 col-md-6 mb-30">
                <div class="categories-items">
                    <div class="cate-images">
                        <a href="<?= base_url('pages/reach/'.$this->lang)?>"><img src="<?= base_url()?>assets/images/inisiatif/reach.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 mb-30">
                <div class="categories-items">
                    <div class="cate-images">
                        <a href="<?= base_url('pages/koper/'.$this->lang)?>"><img src="<?= base_url()?>assets/images/inisiatif/kopper.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end -->
<!-- kolaborator -->
<div id="rs-blog" class="rs-blog main-home pb-20 pt-20 md-pt-20 md-pb-20">
    <div class="container">
        <div class="sec-title3 text-center mb-50">
            <h2 class="title"> KOLABORATOR</h2>
        </div>
        <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true"
            data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false"
            data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1"
            data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="2"
            data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1"
            data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="false"
            data-md-device-dots="false">
            <div class="blog-item">
                <div class="image-part">
                    <img src="assets/images/roemi/1.png" alt="">
                </div>
            </div>
            <div class="blog-item">
                <div class="image-part">
                    <img src="assets/images/roemi/2.png" alt="">
                </div>
            </div>
            <div class="blog-item">
                <div class="image-part">
                    <img src="assets/images/roemi/3.png" alt="">
                </div>
            </div>
        </div>
        <div class="pb-25 pt-25 md-pt-25 md-pb-25">
            <h2 class="readon blue-btn main-home btnMore "><a href="<?= base_url('pages/kolaborator/'.$this->lang)?>" style="color: #fff">More</a></h2>
        </div>
    </div>
</div>
<!-- end -->
<!-- join -->
<div id="rs-blog" class="rs-blog main-home pb-20 pt-20 md-pt-20 md-pb-20">
    <div class="container">
        <div class="sec-title3 text-center mb-50">
            <h2 class="title"> <q>Bergabung dengan ROEMI dan wujudkan masa depan kita lebih cepat.</q></h2>
        </div>
        <div class="pb-25 pt-25 md-pt-25 md-pb-25">
            <h2 class="readon blue-btn main-home btnMore"><a href="<?= base_url('pages/reach/'.$this->lang)?>" style="color: #fff">Join Us</a></h2>
        </div>
    </div>
</div>
</div>
<!-- Main content End -->