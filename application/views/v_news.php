<!-- gallery -->
<div class="main-content">
    <!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        <div class="breadcrumbs-img">
             <img src="<?= base_url()?>assets/images/breadcrumbs/4.jpg" alt="Breadcrumbs Image">
        </div>
        <div class="spesies">
            <div class="container white-wrapper">
                <div class="row text-center">
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/foto/'.$this->lang)?>">Foto</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/video/'.$this->lang)?>">Video</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a
                                href="<?= base_url('pages/audio/'.$this->lang)?>">Audio</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/material/'.$this->lang)?>">Material</a></h3>
                    </div>
                    <div class="col-md-2 br-black">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/news/'.$this->lang)?>">News</a></h3>
                    </div>
                    <div class="col-md-2 ">
                        <h3 class="pub-menu oswald uppercase" style="margin-bottom: 0px;"><a class="active"
                                href="<?= base_url('pages/event/'.$this->lang)?>">Event</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs End -->
        

            <!-- Events Section Start -->
            <div class="rs-event orange-color pt-100 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 mb-60 col-md-6">
                            <div class="event-item">
                                <div class="event-short">
                                   <div class="featured-img">
                                       <img src="<?= base_url()?>assets/images/roemi/6.png" alt="Image">
                                   </div>
                                   <div class="content-part">
                                       <div class="address"><i class="fa fa-map-o"></i> New Margania</div>
                                       <h4 class="title"><a href="#">Spicy Quince And Cranberry Chutney</a></h4>
                                       <p class="text">
                                           Bootcamp Events Description Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...
                                       </p>
                                       <div class="event-btm">
                                           <div class="date-part">
                                               <div class="date">
                                                   <i class="fa fa-calendar-check-o"></i>
                                                   July 24, 2020 
                                               </div>
                                           </div>
                                           <div class="btn-part">
                                               <a href="#">Baca Selengkapnya</a>
                                           </div>
                                       </div>
                                   </div> 
                                </div>
                            </div>
                        </div>   
                        <div class="col-lg-4 mb-60 col-md-6">
                            <div class="event-item">
                                <div class="event-short">
                                   <div class="featured-img">
                                       <img src="<?= base_url()?>assets/images/roemi/2.png" alt="Image">
                                   </div>
                                  
                                   <div class="content-part">
                                       <div class="address"><i class="fa fa-map-o"></i> New Margania</div>
                                       <h4 class="title"><a href="#">Persim, Pomegran, And Massag Kale Salad</a></h4>
                                       <p class="text">
                                           Bootcamp Events Description Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...
                                       </p>
                                       <div class="event-btm">
                                           <div class="date-part">
                                               <div class="date">
                                                   <i class="fa fa-calendar-check-o"></i>
                                                   July 24, 2020 
                                               </div>
                                           </div>
                                           <div class="btn-part">
                                               <a href="#">Baca Selengkapnya</a>
                                           </div>
                                       </div>
                                   </div> 
                                </div>
                            </div>
                        </div>   
                        <div class="col-lg-4 mb-60 col-md-6">
                            <div class="event-item">
                                <div class="event-short">
                                   <div class="featured-img">
                                       <img src="<?= base_url()?>assets/images/roemi/6.png" alt="Image">
                                   </div>
                                  
                                   <div class="content-part">
                                       <div class="address"><i class="fa fa-map-o"></i> New Margania</div>
                                       <h4 class="title"><a href="#">Essential Fall Fruits That Aren’t Apples</a></h4>
                                       <p class="text">
                                           Bootcamp Events Description Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...
                                       </p>
                                       <div class="event-btm">
                                           <div class="date-part">
                                               <div class="date">
                                                   <i class="fa fa-calendar-check-o"></i>
                                                   July 24, 2020 
                                               </div>
                                           </div>
                                           <div class="btn-part">
                                               <a href="#">Baca Selengkapnya</a>
                                           </div>
                                       </div>
                                   </div> 
                                </div>
                            </div>
                        </div>   
                    </div>
                </div> 
            </div>
            <!-- Events Section End --> 
        </div> 
<!-- end gallery -->