<?php
class M_db extends CI_Model
{
    function get_pages($number = 10, $start = 0)
    {
        $this->db->select();
        $this->db->from('pages');
        $this->db->where('active',1);
        $this->db->order_by('date_added','desc');
        $this->db->group_by('container');
        $this->db->limit($number, $start);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_posts($number = 10, $start = 0)
    {
        $this->db->select();
        $this->db->from('posts');
        $this->db->where('active',1);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->order_by('date_added','desc');
        $this->db->limit($number, $start);
        $query = $this->db->get();
        return $query->result_array();
    }

	function search_posts($query)
	{
		$this->db->select();
		$this->db->from('posts');
		$this->db->like("post_title", $query, 'both');
		$this->db->or_like("post", $query, 'both');
		$this->db->order_by('date_added', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}
    function get_my_post_count()
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->select()->from('posts')->where('active',1);
        $query = $this->db->get();
        return $query->num_rows;
    }
    function get_post_count()
    {
        $this->db->select()->from('posts')->where('active',1);
        $query = $this->db->get();
        return $query->num_rows;
    }
    function get_post($post_id)
    {
        $this->db->select();
        $this->db->from('posts');
        $this->db->where(array('active'=>1,'post_id'=>$post_id,'user_id' =>$this->session->userdata('user_id')));
        $this->db->order_by('date_added','desc');
        $query = $this->db->get();
        return $query->first_row('array');
    }
    function get_one_page($post_id)
    {
        $this->db->select();
        $this->db->from('pages');
        $this->db->where(array('active'=>1,'post_id'=>$post_id,'user_id' =>$this->session->userdata('user_id')));
        $this->db->order_by('date_added','desc');
        $query = $this->db->get();
        return $query->first_row('array');
    }
    function getByContainer($cid, $lang)
    {
        $this->db->select();
        $this->db->from('pages');
        $this->db->where(array('container'=>$cid, "language" => $lang));

        $query = $this->db->get();
        return $query->first_row('array');
    }
    function getByType($type, $lang = "id")
    {
        $this->db->select();
        $this->db->from('pages');
        $this->db->where(array('type'=>$type, "language" => $lang));
        // $this->db->group_by('container');
        $query = $this->db->get();
        return $query->result_array();
    }
    function insert_post($data)
    {
        $this->db->insert('posts',$data);
        return $this->db->insert_id();
    }
    function insert_pages($data)
    {
        $this->db->insert('pages',$data);
        return $this->db->insert_id();
    }
    
    function update_post($post_id, $data)
    {
        $this->db->where('post_id',$post_id);
        $this->db->update('posts',$data);
    }
    
    
    function update_page($post_id, $data)
    {
        $this->db->where('post_id',$post_id);
        $this->db->update('pages',$data);
    }
    
    function delete_post($post_id)
    {
        $this->db->where('post_id',$post_id);
        $this->db->delete('posts');
    }
}
