<?php
class M_contact extends CI_Model
{
    function list()
    {
        $this->db->select();
        $this->db->from('contact');
        $this->db->order_by('created_at','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function create($data)
    {
        $this->db->insert('contact',$data);
        return $this->db->insert_id();
    }
    
    
    function update_page($post_id, $data)
    {
        $this->db->where('post_id',$post_id);
        $this->db->update('pages',$data);
    }
    
    function delete_post($post_id)
    {
        $this->db->where('post_id',$post_id);
        $this->db->delete('posts');
    }
}
