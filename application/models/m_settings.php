<?php
class M_settings extends CI_Model
{
    function create($data)
    {
        $this->db->insert('settings', $data);
        return $this->db->insert_id();;
    }
    function update($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('settings',$data);
    }


    function list_settings($number = 10, $start = 0)
    {
        $this->db->select();
        $this->db->from('settings');
        $this->db->limit($number, $start);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_settings($id)
    {
        $this->db->select();
        $this->db->from('settings');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function getByName($name)
    {
        $this->db->select();
        $this->db->from('settings');
        $this->db->where("name", $name);
        $query = $this->db->get();
        return $query->row_array();
    }
    function updateByName($name, $data)
    {
        $this->db->where('name',$name);
        $this->db->update('settings',$data);
    }
}
