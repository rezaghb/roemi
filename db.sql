-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2022 at 09:18 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roemi`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(400) NOT NULL,
  `email` varchar(400) NOT NULL,
  `subject` varchar(400) DEFAULT NULL,
  `message` varchar(4000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `message`, `created_at`) VALUES
(1, 'Reza', 'ycared@gmail.com', 'Test contact', 'please provive me the price of class privete', '2022-02-05 08:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `post_id` int(11) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `post_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `container` varchar(45) DEFAULT NULL,
  `content1` text,
  `content2` text,
  `content3` text,
  `content4` text,
  `content5` text,
  `content6` text,
  `content7` text,
  `content8` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`post_id`, `type`, `post_title`, `active`, `date_added`, `user_id`, `language`, `container`, `content1`, `content2`, `content3`, `content4`, `content5`, `content6`, `content7`, `content8`) VALUES
(1, '', 'Menciptakan dunia yang adil dan setara di mana orang merasa aman untuk mengeksplorasi, belajar, tumbuh, berinovasi, dan berkolaborasi menuju masa depan yang lebih baik.', 1, '2022-01-30 03:41:04', 8, 'id', 'b991d57f9e35813ca5b90dab68b5f276', 'Yayasan Roemah Inspirit (ROEMI) is a non-profit organization that develops various social innovation in the areas of communication, facilitation, and capacity building for movement social justice in Indonesia and the world. ROEMI believes communication is everything. Communication is the key to more change and be at the heart of developing a more equal and just world.', '389622440_homebanne.jpg', '0', '0', '0', NULL, NULL, NULL),
(2, '', 'Menciptakan dunia yang adil dan setara di mana orang merasa aman untuk mengeksplorasi, belajar, tumbuh, berinovasi, dan berkolaborasi menuju masa depan yang lebih baik.', 1, '2022-01-30 03:41:04', 8, 'en', 'b991d57f9e35813ca5b90dab68b5f276', 'Yayasan Roemah Inspirit (ROEMI) is a non-profit organization that develops various social innovation in the areas of communication, facilitation, and capacity building for movement social justice in Indonesia and the world. ROEMI believes communication is everything. Communication is the key to more change and be at the heart of developing a more equal and just world.', '1316230606_homebanne.jpg', '0', '0', '0', NULL, NULL, NULL),
(3, '', 'Tentang Kami', 1, '2022-01-30 03:42:38', 8, 'id', '87dc9560c7a13615a38eed3e426a8418', 'Yayasan Roemah Inspirit (ROEMI) is a non-profit organization that develops various social innovation in the areas of communication, facilitation, and capacity building for movement social justice in Indonesia and the world. ROEMI believes communication is everything. Communication is the key to more change and be at the heart of developing a more equal and just world.', 'Menciptakan dunia yang adil dan setara di mana orang merasa aman untuk mengeksplorasi, belajar, tumbuh, berinovasi, dan berkolaborasi menuju masa depan yang lebih baik.\r\n', 'Menciptakan dan mempercepat kapasitas individu dan kolektif Organisasi Masyarakat Sipil, Komunitas Pemuda, dan Kelompok Perempuan untuk mewujudkan tujuan dan memperkuat dampak mereka untuk membuat dunia yang lebih baik  lebih cepat terwujud.', 'Connection. Collaboration. Change.', '0', NULL, NULL, NULL),
(4, '', 'About Us', 1, '2022-01-30 03:42:38', 8, 'en', '87dc9560c7a13615a38eed3e426a8418', 'Yayasan Roemah Inspirit (ROEMI) is a non-profit organization that develops various social innovation in the areas of communication, facilitation, and capacity building for movement social justice in Indonesia and the world. ROEMI believes communication is everything. Communication is the key to more change and be at the heart of developing a more equal and just world.', 'Menciptakan dunia yang adil dan setara di mana orang merasa aman untuk mengeksplorasi, belajar, tumbuh, berinovasi, dan berkolaborasi menuju masa depan yang lebih bai', 'Menciptakan dan mempercepat kapasitas individu dan kolektif Organisasi Masyarakat Sipil, Komunitas Pemuda, dan Kelompok Perempuan untuk mewujudkan tujuan dan memperkuat dampak mereka untuk membuat dunia yang lebih baik  lebih cepat terwujud.', 'Connection. Collaboration. Change.', '0', NULL, NULL, NULL),
(5, NULL, 'pekerjaan', 1, '2022-01-30 03:44:00', 8, 'id', 'dfdfe14c07f74c4d98c4ef4f5a556dd9', 'reach indineis', '', '', '', '', NULL, NULL, NULL),
(6, NULL, 'our works', 1, '2022-01-30 03:44:00', 8, 'en', 'dfdfe14c07f74c4d98c4ef4f5a556dd9', 'rach inggiris', '', '', '', '', NULL, NULL, NULL),
(7, 'resources', 'sumber daya', 1, '2022-01-30 03:44:12', 8, 'id', 'ece73802ab509a658a8ef3a54fdfe068', '0', '0', '0', '0', '0', NULL, NULL, NULL),
(8, 'resources', 'resources', 1, '2022-01-30 03:44:12', 8, 'en', 'ece73802ab509a658a8ef3a54fdfe068', '0', '0', '0', '0', '0', NULL, NULL, NULL),
(9, '', 'rekanan', 1, '2022-01-30 03:44:30', 8, 'id', 'e15f1bf8049a42b4785ff9c3e7f53c86', '0', '0', '0', '0', '0', NULL, NULL, NULL),
(10, '', 'partners', 1, '2022-01-30 03:44:30', 8, 'en', 'e15f1bf8049a42b4785ff9c3e7f53c86', '0', '0', '0', '0', '0', NULL, NULL, NULL),
(11, NULL, 'melibatkan', 1, '2022-01-30 03:45:02', 8, 'id', '440564e18b5e314dd903c2f5607397a0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, 'involved', 1, '2022-01-30 03:45:02', 8, 'en', '440564e18b5e314dd903c2f5607397a0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, 'hubungi kami', 1, '2022-01-30 03:45:40', 8, 'id', '2f87a4b49adab72bb3e4e7c5be0d02a2', 'hubungi kami1', 'hubungi kami2', 'hubungi kami3', 'hubungi kami4', 'hubungi kami5', NULL, NULL, NULL),
(14, NULL, 'get in touch', 1, '2022-01-30 03:45:40', 8, 'en', '2f87a4b49adab72bb3e4e7c5be0d02a2', 'get in touch1', 'get in touch2', 'get in touch3', 'get in touch4', 'get in touch5', NULL, NULL, NULL),
(15, '', 'jadi member', 1, '2022-01-31 09:24:59', 8, 'id', 'cb6e4c192250e0cd20bc7dfbc94b1f6e', 'roemi.id', '0', '0', '0', '0', NULL, NULL, NULL),
(16, '', 'become member', 1, '2022-01-31 09:25:00', 8, 'en', 'cb6e4c192250e0cd20bc7dfbc94b1f6e', 'roemi.id', '0', '0', '0', '0', NULL, NULL, NULL),
(17, '', 'subscribe', 1, '2022-01-31 09:30:08', 8, 'id', '84d9015ed888682904e2a60aacb55cce', 'yt', 'yt.com/ok', '0', '0', '0', NULL, NULL, NULL),
(18, '', 'subscribe', 1, '2022-01-31 09:30:08', 8, 'en', '84d9015ed888682904e2a60aacb55cce', 'yt.com', 'yt.com/ok', '0', '0', '0', NULL, NULL, NULL),
(19, '', 'donor pribadi', 1, '2022-01-31 09:32:08', 8, 'id', '0ce90809ee7460fcce92fcbf3ac263d2', '2346283764', 'reza mukti', 'mandiri', '0', '0', NULL, NULL, NULL),
(20, '', 'donor pribadi', 1, '2022-01-31 09:32:08', 8, 'en', '0ce90809ee7460fcce92fcbf3ac263d2', '1288771604_qr-code.png', '0', '0', '0', '0', NULL, NULL, NULL),
(21, '', 'donor lembaga', 1, '2022-01-31 09:32:39', 8, 'id', '99576a8a1652c9bcc9930e2d7d385ead', '089520020420', '677370692_AppNet_Network_2022.pdf', '0', '0', '0', NULL, NULL, NULL),
(22, '', 'donor lembaga', 1, '2022-01-31 09:32:39', 8, 'en', '99576a8a1652c9bcc9930e2d7d385ead', '089520020020', '1321959002_AppNet_Network_2022.pdf', '0', '0', '0', NULL, NULL, NULL),
(23, NULL, 'faq', 1, '2022-01-31 09:45:26', 8, 'id', '349d8a8ed0ab9f2aeb9e1f76ed4111e3', 'faq', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, NULL, 'faq', 1, '2022-01-31 09:45:26', 8, 'en', '349d8a8ed0ab9f2aeb9e1f76ed4111e3', 'faq', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '', 'media', 1, '2022-01-31 10:13:09', 8, 'id', 'fd3a0cdd31b1e03a7540c5ee766ac385', '2064811343_herbal_angkung_yen_full01_hkogkepa.jpg', '0', '0', '0', '0', NULL, NULL, NULL),
(26, '', 'media', 1, '2022-01-31 10:13:09', 8, 'en', 'fd3a0cdd31b1e03a7540c5ee766ac385', '1291242046_herbal_angkung_yen_full01_hkogkepa.jpg', '0', '0', '0', '0', NULL, NULL, NULL),
(27, NULL, 'event', 1, '2022-01-31 10:16:14', 8, 'id', 'd962c3ca56c3ac25725bb76882845e3d', 'event', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, NULL, 'event', 1, '2022-01-31 10:16:14', 8, 'en', 'd962c3ca56c3ac25725bb76882845e3d', 'event', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'confirmation', 'reza', 1, '2022-01-31 10:47:24', 1, NULL, NULL, 'mandiri', '93485934', '200000', 'bukti.png', NULL, 'Approved', NULL, NULL),
(38, '', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', 0, '2022-02-01 12:11:27', 0, 'id', '87a00519bc3bcbf74989854fe3ff09da', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', 'https://open.spotify.com/show/0uks3W6oiTblt8UwLGRyss?si=314284958d7743a7', '0', '0', '0', NULL, NULL, NULL),
(39, '', 'koper', 0, '2022-02-01 12:11:27', 0, 'en', '87a00519bc3bcbf74989854fe3ff09da', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', 'https://open.spotify.com/show/0uks3W6oiTblt8UwLGRyss?si=314284958d7743a7', '0', '0', '0', NULL, NULL, NULL),
(40, '', 'reach', 0, '2022-02-01 12:20:55', 0, 'id', '514d8f76d8370fafa68dffc984ad85a9', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', 'lintoreach.com', '0', '0', '0', NULL, NULL, NULL),
(41, '', 'reach', 0, '2022-02-01 12:20:55', 0, 'en', '514d8f76d8370fafa68dffc984ad85a9', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', 'lintoreach.com', '0', '0', '0', NULL, NULL, NULL),
(46, 'partners-sosmed', 'instagram', 0, '2022-02-02 03:22:38', 0, 'id', 'ac1d0785253ba6f1f5dff1b170da7f66', 'ig.com', '0', '0', '0', '0', NULL, NULL, NULL),
(47, 'partners-sosmed', 'facebook', 0, '2022-02-02 03:22:39', 0, 'en', 'ac1d0785253ba6f1f5dff1b170da7f66', '', '0', '0', '0', '0', NULL, NULL, NULL),
(50, 'event', 'roemi seminar', 0, '2022-02-02 06:24:48', 0, 'id', '61c297064324e29be2b14031e178765d', '2022-02-02', '2022-02-18', '', '0', '0', NULL, NULL, NULL),
(51, 'event', 'seminar baca', 0, '2022-02-02 06:24:48', 0, 'en', '61c297064324e29be2b14031e178765d', '2022-02-02', '2022-02-03', '', '0', '0', NULL, NULL, NULL),
(56, 'confirmation', 'reza', 0, '2022-02-02 08:19:17', 0, 'id', '09f56318b335957816ca14fb46373773', 'bca', '234234', '5000000', '1988042363_herbal_angkung_yen_full01_hkogkepa.jpg', '2022-02-10', 'Approved', NULL, NULL),
(57, 'confirmation', '0', 0, '2022-02-02 08:19:17', 0, 'en', '09f56318b335957816ca14fb46373773', '0', '0', '0', '0', '0', NULL, NULL, NULL),
(58, 'event', 'test', 1, '2022-02-03 11:24:00', 0, 'id', '89f2c200e785e132c49a5214fc510e59', '2022-02-04', '2022-02-04', '', '0', '0', NULL, NULL, NULL),
(59, 'event', 'resr', 1, '2022-02-03 11:24:00', 0, 'en', '89f2c200e785e132c49a5214fc510e59', '2022-02-24', '2022-02-11', '', '0', '0', NULL, NULL, NULL),
(64, 'reach-news', 'news', 1, '2022-02-04 07:02:09', 0, 'id', '500d58be97974b92caca9aede1b8ebab', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', '0', '0', '0', '0', NULL, NULL, NULL),
(65, 'reach-news', 'news', 1, '2022-02-04 07:02:09', 0, 'en', '500d58be97974b92caca9aede1b8ebab', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', '0', '0', '0', '0', NULL, NULL, NULL),
(66, 'resources-sosmed', 'facebook', 1, '2022-02-04 07:08:04', 0, 'id', 'e3ad1a4cd66526ac2835718d37e0dd5f', 'fb1.com', '0', '0', '0', '0', NULL, NULL, NULL),
(67, 'resources-sosmed', 'facebook', 1, '2022-02-04 07:08:05', 0, 'en', 'e3ad1a4cd66526ac2835718d37e0dd5f', '', '0', '0', '0', '0', NULL, NULL, NULL),
(68, 'resources-sosmed', 'twitter', 1, '2022-02-04 07:08:54', 0, 'id', '30096bd368b5fce20bf6809ff092a639', 'twiterr.com', '0', '0', '0', '0', NULL, NULL, NULL),
(69, 'resources-sosmed', 'facebook', 1, '2022-02-04 07:08:55', 0, 'en', '30096bd368b5fce20bf6809ff092a639', '', '0', '0', '0', '0', NULL, NULL, NULL),
(76, 'event', 'test', 1, '2022-02-04 07:47:53', 0, 'id', '1a451b9311cc21d37491d4cbaaf2f9c6', '2022-02-04', '2022-02-10', '', '0', '0', NULL, NULL, NULL),
(77, 'event', '', 1, '2022-02-04 07:47:53', 0, 'en', '1a451b9311cc21d37491d4cbaaf2f9c6', '', '', '', '0', '0', NULL, NULL, NULL),
(78, 'resources-sosmed', 'youtube', 1, '2022-02-04 08:19:04', 0, 'id', '58fe355a0027ba373439f79312c29a02', 'sdfsdf', '0', '0', '0', '0', NULL, NULL, NULL),
(79, 'resources-sosmed', 'facebook', 1, '2022-02-04 08:19:04', 0, 'en', '58fe355a0027ba373439f79312c29a02', '', '0', '0', '0', '0', NULL, NULL, NULL),
(82, 'confirmation', 'sfd', 1, '2022-02-04 09:09:12', 0, 'id', 'b969d097599773077378dd891466ed3c', 'sdf', 'sdf', '5000000', '25616638_vps_list.png', '2022-02-17', NULL, NULL, NULL),
(83, 'confirmation', '0', 1, '2022-02-04 09:09:12', 0, 'en', 'b969d097599773077378dd891466ed3c', '0', '0', '0', '0', '0', NULL, NULL, NULL),
(84, 'confirmation', 'reza', 1, '2022-02-04 09:09:40', 0, 'id', '4647c7bd4c46a28406f96400f11d342a', 'ceo', 'facebook', '5000000', '1406359063_vps_list.png', '2022-02-16', NULL, NULL, NULL),
(85, 'confirmation', '0', 1, '2022-02-04 09:09:40', 0, 'en', '4647c7bd4c46a28406f96400f11d342a', '0', '0', '0', '0', '0', NULL, NULL, NULL),
(86, 'personil', 'Dr. Aji Rama', 1, '2022-02-05 07:38:10', 0, 'id', 'dba43c67a56b7019bbb38481f95767ec', 'CEO', 'https://www.facebook.com/aji', 'https://www.instagram.com/aji', 'https://www.twitter.com/aji', '1299810983_member.png', NULL, NULL, NULL),
(87, 'personil', '', 1, '2022-02-05 07:38:11', 0, 'en', 'dba43c67a56b7019bbb38481f95767ec', '', '', '', '0', '0', NULL, NULL, NULL),
(88, 'reach-news', 'lorem ipsum dolor', 1, '2022-02-05 07:49:17', 0, 'id', '548916dba381ca995f811386ead7adf4', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', '0', '0', '0', '0', NULL, NULL, NULL),
(89, 'reach-news', 'eng lorem ipsum', 1, '2022-02-05 07:49:17', 0, 'en', '548916dba381ca995f811386ead7adf4', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', '0', '0', '0', '0', NULL, NULL, NULL),
(90, 'partners-logo', '139471276_avpn_logo_fordfoundation-1.png', 1, '2022-02-05 08:02:17', 0, 'id', 'faf77f624c70071a194e0990dbb64cbd', 'PT Pertamina', '0', '0', '0', '0', NULL, NULL, NULL),
(91, 'partners-logo', '', 1, '2022-02-05 08:02:18', 0, 'en', 'faf77f624c70071a194e0990dbb64cbd', '', '0', '0', '0', '0', NULL, NULL, NULL),
(92, 'partners-logo', '1842904527_avpn_logo_fordfoundation-1.png', 1, '2022-02-05 08:04:24', 0, 'id', 'e367cf589e1927186c5164dbdc8fa51a', 'FORD FOUNDATION', '0', '0', '0', '0', NULL, NULL, NULL),
(93, 'partners-logo', '', 1, '2022-02-05 08:04:25', 0, 'en', 'e367cf589e1927186c5164dbdc8fa51a', '', '0', '0', '0', '0', NULL, NULL, NULL),
(94, 'personil', 'Prof. Renald Khasali', 1, '2022-02-05 08:21:00', 0, 'id', '1af03986b0c8224a3f713933810aec46', 'CTO', 'facebook', '1299810983_member.png', '0', '1299810983_member.png', NULL, NULL, NULL),
(95, 'personil', 'Prof. Renald Khasali', 1, '2022-02-05 08:21:01', 0, 'en', '1af03986b0c8224a3f713933810aec46', 'CTO', 'facebook.com', '1299810983_member.png', '0', '0', NULL, NULL, NULL),
(96, 'event', 'Event tgl 28 Maret', 1, '2022-02-06 07:50:50', NULL, 'id', 'eb824090ad0e4e15b39a095fb35462d4', '2022-03-28', '2022-03-31', '', '0', '0', NULL, NULL, NULL),
(97, 'event', 'Event tgl 28 Maret', 1, '2022-02-06 07:50:50', NULL, 'en', 'eb824090ad0e4e15b39a095fb35462d4', '2022-03-28', '2022-03-31', '', '0', '0', NULL, NULL, NULL),
(98, 'faq', 'Roemi itu apa sih?', 1, '2022-02-07 07:06:47', NULL, 'id', '1036a3fd81fa38aa745858df64231a70', 'Roemi itu adalah', '0', '0', '0', '0', NULL, NULL, NULL),
(99, 'faq', 'Whats ROEMI?', 1, '2022-02-07 07:06:47', NULL, 'en', '1036a3fd81fa38aa745858df64231a70', 'ROEMI is poem', '0', '0', '0', '0', NULL, NULL, NULL),
(100, '', 'facebook.com/roemi', 1, '2022-02-07 07:19:29', NULL, 'id', '16975d4fc3dd4c858cf9a3c3dfc2ba92', 'instagram.com/roemi', 'yotube.com/roemi', '0', '0', '0', NULL, NULL, NULL),
(101, '', 'get in sosmed', 1, '2022-02-07 07:19:29', NULL, 'en', '16975d4fc3dd4c858cf9a3c3dfc2ba92', 'get in sosmed', '0', '0', '0', '0', NULL, NULL, NULL),
(102, 'event', 'ada  imagenya', 1, '2022-02-14 09:44:47', NULL, 'id', '6766a06ad79097592c9397a5957805b9', '2022-02-17', '2022-02-28', '1104289151_pertamina-1.png', '0', '0', NULL, NULL, NULL),
(103, 'event', '', 1, '2022-02-14 09:44:47', NULL, 'en', '6766a06ad79097592c9397a5957805b9', '', '', '', '0', '0', NULL, NULL, NULL),
(104, 'event', 'ada  imagenyax', 1, '2022-02-14 09:45:43', NULL, 'id', 'db1dcd0940d771faf529847f011316db', '2022-02-17', '2022-02-28', '1104289151_pertamina-1.png', '0', '0', NULL, NULL, NULL),
(105, 'event', 'ada  imagenyax', 1, '2022-02-14 09:45:43', NULL, 'en', 'db1dcd0940d771faf529847f011316db', '', '', '', '0', '0', NULL, NULL, NULL),
(106, 'reach-news', 'news', 1, '2022-02-14 09:57:34', NULL, 'id', 'd8bee1cef23662408cb7feb50f5cf454', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', '0', '0', '0', '0', NULL, NULL, NULL),
(107, 'reach-news', 'news', 1, '2022-02-14 09:57:35', NULL, 'en', 'd8bee1cef23662408cb7feb50f5cf454', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs', '0', '0', '0', '0', NULL, NULL, NULL),
(108, 'personil', 'Dr. Aji Ramax 123', 1, '2022-02-14 10:50:38', NULL, 'id', 'fa86166b857aa781d453238401258d66', 'CEO', 'facebook', '650393857_member.png', '0', '1299810983_member.png', NULL, NULL, NULL),
(109, 'personil', '', 1, '2022-02-14 10:50:38', NULL, 'en', 'fa86166b857aa781d453238401258d66', '', '', '', '0', '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(400) DEFAULT NULL,
  `value` varchar(4000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'logo', 'http://roemi.id/assets/images/roemi.png'),
(2, 'sitename', 'ROEMI'),
(3, 'fb', 'https://www.facebook.com/'),
(4, 'address', 'Grha Tirtadi Jalan Pangeran Antasari No. 18A Unit 405 Jakarta Selatan - 12150'),
(5, 'telp', '021-27656438'),
(6, 'email', 'hello@roemi.id'),
(7, 'twitter', 'https://www.twitter.com/'),
(8, 'ig', 'https://www.instagram.com/'),
(9, 'visitor', '681');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(4) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` enum('admin','author','user') NOT NULL,
  `counter` tinyint(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `username`, `password`, `user_type`, `counter`) VALUES
(4, 'admin@gmail.com', 'admin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'admin', NULL),
(10, 'reza@gmail.com', 'reza', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'admin', 0),
(11, 'anggun@example.com', 'anggun', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'admin', 0),
(12, 'anggun2@example.com', 'anggun', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'admin', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
